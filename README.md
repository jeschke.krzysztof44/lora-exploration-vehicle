# Lora exploration vehicle

Exploration vehicle capable of measuring: temperature, humidity, atmospheric pressure and light intensity. The project consists of several main parts, which are described below.

![vehicle.jpg](./img/vehicle.jpg)

## Sensors
Three sensors are responsible for measuring and sending data (temperature, humidity, atmospheric pressure and light intensity) to NodeMCU-32S microcontroller using I2C protocol for communication. All devices are mounted on the PCB prototype board. 
There are also four ultrasonic distance sensors used to collision avoidance. All collected data is sent to Heltec LoRa microcontroller using Wi-Fi.

![sensors.jpg](./img/sensors.jpg)

## Camera
ESP32-CAM is used to take pictures of objects around the vehicle. The camera and powering module are mounted on a pan/tilt servomotors kit, which allows a user to move the camera. Current image is sent to Heltec LoRa microcontroller using Wi-Fi.

![camera.jpg](./img/camera.jpg)

## DC Motors
Four DC motors are used to move the vehicle. They are connected to omni wheels which allow user to move the vehicle to any direction. NodeMCU-32S microcontroller is responsible for controlling speed and direction using two L298N DC motors controllers. 

![motors.jpg](./img/motors.jpg)

## LoRa
All data collected by the vehicle is sent to a LoRa gateway and then to a LoRa application server. Capability of sending images using LoRa hasn't been tested yet.

## Controll
User can control the vehicle using GUI (Python). It allows user to send direct control messages such as: "take picture", "move left", "move right" etc. User can also read data collected by the module. The program uses MQTT protocol to connect with LoRa Application server.
