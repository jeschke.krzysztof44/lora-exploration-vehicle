/*
    Camera.h
    
    Author:     Krzysztof Jeschke
    Created:    26.06.2022
*/

#pragma once

#ifndef __Camera_H__
#define __Camera_H__

#include "esp_camera.h"
#include <mbedtls/base64.h>

#include "../../include/config.h"


// Controls camera pan and tilt moves using two TowerPro MG995 servomotors connectd with each other.
class Camera
{

// variables
public:
    size_t jpgBufferLen;
    uint8_t * jpgBuffer = NULL;
    
private:
    static constexpr const char *TAG = "Camera";

    camera_config_t camera_config;

// methods
public:
    Camera();
    ~Camera();

    esp_err_t init();

    void printfImgBase64(const camera_fb_t *pic);
    void takePicture();
    void freeJpgBuffer();

private:
    void cameraConfigInit();
};

#endif