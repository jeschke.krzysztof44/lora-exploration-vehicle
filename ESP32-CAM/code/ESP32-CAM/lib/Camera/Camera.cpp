/*
    Camera.cpp
    
    Author:     Krzysztof Jeschke
    Created:    26.06.2022
*/

#include "Camera.h"

Camera::Camera()
{
    
}

Camera::~Camera()
{
}

esp_err_t Camera::init()
{
    cameraConfigInit();

    //initialize the camera
    esp_err_t err = esp_camera_init(&camera_config);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Camera Init Failed");
        return err;
    }

    return ESP_OK;
}

void Camera::cameraConfigInit()
{
this->camera_config.pin_pwdn = CAM_PIN_PWDN;
this->camera_config.pin_reset = CAM_PIN_RESET;
this->camera_config.pin_xclk = CAM_PIN_XCLK;
this->camera_config.pin_sscb_sda = CAM_PIN_SIOD;
this->camera_config.pin_sscb_scl = CAM_PIN_SIOC;

this->camera_config.pin_d7 = CAM_PIN_D7;
this->camera_config.pin_d6 = CAM_PIN_D6;
this->camera_config.pin_d5 = CAM_PIN_D5;
this->camera_config.pin_d4 = CAM_PIN_D4;
this->camera_config.pin_d3 = CAM_PIN_D3;
this->camera_config.pin_d2 = CAM_PIN_D2;
this->camera_config.pin_d1 = CAM_PIN_D1;
this->camera_config.pin_d0 = CAM_PIN_D0;
this->camera_config.pin_vsync = CAM_PIN_VSYNC;
this->camera_config.pin_href = CAM_PIN_HREF;
this->camera_config.pin_pclk = CAM_PIN_PCLK;

//XCLK 20MHz or 10MHz for OV2640 double FPS (Experimental)
this->camera_config.xclk_freq_hz = 10000000;
this->camera_config.ledc_timer = LEDC_TIMER_0;
this->camera_config.ledc_channel = LEDC_CHANNEL_0;

this->camera_config.pixel_format = PIXFORMAT_RGB565; //YUV422;GRAYSCALE;RGB565;JPEG
this->camera_config.frame_size = FRAMESIZE_SVGA;    //QQVGA-UXGA Do not use sizes above QVGA when not JPEG

this->camera_config.jpeg_quality = 12; //0-63 lower number means higher quality
this->camera_config.fb_count = 1;       //if more than one; i2s runs in continuous modethis->camera_config. Use only with JPEG
this->camera_config.grab_mode = CAMERA_GRAB_LATEST;
}

void Camera::printfImgBase64(const camera_fb_t *pic)
{
    uint8_t *outbuffer = NULL;
    size_t outsize = 0;

    printf("\nTUUU\n");

    if (PIXFORMAT_JPEG != pic->format) {
        fmt2jpg(pic->buf, pic->width * pic->height * 2, pic->width, pic->height, pic->format, 50, &outbuffer, &outsize);
        printf("\nTUUU1\n");
    } else {
        outbuffer = pic->buf;
        outsize = pic->len;
        printf("\nTUUU2\n");
    }
printf("\nTUUU3\n");
    // uint8_t *base64_buf = (uint8_t *)calloc(1, outsize * 4);
    // if (NULL != base64_buf) {
    //     size_t out_len = 0;
    //     mbedtls_base64_encode(base64_buf, outsize * 4, &out_len, outbuffer, outsize);
    //     printf("%s\n", base64_buf);
    //     free(base64_buf);
    //     if (PIXFORMAT_JPEG != pic->format) {
    //         free(outbuffer);
    //     }
    // } else {
    //     ESP_LOGE(TAG, "malloc for base64 buffer failed");
    // }
    // printf("\nTUUU\n");    
}

void Camera::takePicture()
{
    ESP_LOGI(TAG, "Taking picture...");
    camera_fb_t *pic = esp_camera_fb_get();

    // use pic->buf to access the image
    ESP_LOGI(TAG, "Picture taken! Its size was: %zu bytes", pic->len);
    // printfImgBase64(pic);

    bool isConverted = frame2jpg(pic, 80, &this->jpgBuffer, &this->jpgBufferLen);
    if(!isConverted) ESP_LOGE(TAG, "failed to convert");
    printf("JPG LEN: %i\n", this->jpgBufferLen);

    esp_camera_fb_return(pic);

    // vTaskDelay(5000 / portTICK_RATE_MS);
}

void Camera::freeJpgBuffer()
{
    free(this->jpgBuffer);
}