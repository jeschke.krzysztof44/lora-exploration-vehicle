/*
    NetworkClient.h
    
    Author:     Krzysztof Jeschke
    Created:    25.04.2022
*/

#pragma once

#ifndef __NetworkClient_H__
#define __NetworkClient_H__

#include <string.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "lwip/err.h"
#include "lwip/sockets.h"

#include <mutex>

#include "../../include/config.h"
#include "../../include/esp_cam_private.h"

#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_WPA2_PSK

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

volatile static EventGroupHandle_t s_wifi_event_group;

class NetworkClient
{

// variables
public:
    char receiveBuffer[TCP_RECEIVE_BUFFER_SIZE];
    uint8_t receivedFrameSize = 0;

    union
    {
        int intData;
        uint8_t uint8_t_Data[sizeof(int)];
    } int_uint8_t_arr;    

private:
    static constexpr const char *TAG = "NetworkClient";

    int currentSocket;

    bool enableSending = false;
    bool enableReceiving = false;


// methods
public:
    NetworkClient();
    ~NetworkClient();

    esp_err_t init();
    void wifi_init_sta(void);
    void tcp_client_task();

    esp_err_t sendData(uint8_t data[], int dataSize);
    esp_err_t sendImage(uint8_t data[], int dataSize);
    esp_err_t receiveData();
    void createSocket();
    void reconnect();

private:
    static void event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data);
    void closeSocket();

    esp_err_t internalReceive();
    esp_err_t internalSend();    
};

#endif