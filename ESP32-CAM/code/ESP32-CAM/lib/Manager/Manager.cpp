/*
    Manager.cpp
    
    Author:     Krzysztof Jeschke
    Created:    31.05.2022
*/

#include "Manager.h"

Manager::Manager()
{
}

Manager::~Manager()
{
}

esp_err_t Manager::init()
{
    esp_err_t ret;

    ret = this->networkClient.init();
    CHECK(TAG, ret, "networkClient init");    

    this->networkClient.wifi_init_sta();
    this->networkClient.createSocket();

    uint8_t myAddress[1] = {Manager::MY_ADDRESS};
    ESP_LOGI(TAG, "Sending my address");
    sendData(myAddress, 1);

    //gimbal.init();
    //gimbal.goToZero();
    camera.init();

    
    //camera.takePicture();
    //camera.freeJpgBuffer();

    return ESP_OK;
}

void Manager::run()
{
    while(1)
    {
        printf("Waiting for commands\n");
        receiveData();

        if (isNewData())
        {
            runReceivedCommands();
        }
    }
}

void Manager::receiveData()
{   
    if (this->networkClient.receiveData() != ESP_OK) this->networkClient.reconnect();
}

void Manager::sendData(uint8_t data[], int dataSize)
{
    if (this->networkClient.sendData(data, dataSize) != ESP_OK) this->networkClient.reconnect();
}

bool Manager::isNewData()
{
    return this->networkClient.receivedFrameSize != 0 ? true : false;
}

void Manager::runReceivedCommands()
{
    uint8_t responseBuffer[TCP_RESPONSE_BUFFER_SIZE];
    uint32_t currentBufferIndex = 0;
    responseBuffer[currentBufferIndex++] = Manager::MY_ADDRESS;

    float gimbalAngle;
    esp_err_t errorTest;

    for(uint32_t i = 0; i < this->networkClient.receivedFrameSize; i++)
    {
        switch (this->networkClient.receiveBuffer[i])
        {
        case Manager::TAKE_PHOTO:
        {
            printf("TAKING PHOTO");
            this->camera.takePicture();
            this->camera.freeJpgBuffer();
            this->camera.takePicture();
            this->networkClient.sendImage(this->camera.jpgBuffer, this->camera.jpgBufferLen);
            //sendData(this->camera.jpgBuffer, this->camera.jpgBufferLen);
            //appendBuffer(responseBuffer, currentBufferIndex, this->camera.jpgBuffer, this->camera.jpgBufferLen);
            this->camera.freeJpgBuffer();
            this->networkClient.receivedFrameSize = 0; 
            return;
            break;
        }
        case Manager::MOVE_GIMBAL_UP:
        {
            gimbalAngle = (float)this->networkClient.receiveBuffer[++i];
            printf("GIMBAL UP ANGLE: %f \n", gimbalAngle);
            errorTest = this->gimbal.addAngle((float)(-gimbalAngle), gimbalRotations::Tilt);
            break;
        }     
        case Manager::MOVE_GIMBAL_DOWN:
        {
            gimbalAngle = (float)this->networkClient.receiveBuffer[++i];
            printf("GIMBAL DOWN ANGLE: %f \n", gimbalAngle);
            errorTest = this->gimbal.addAngle((float)gimbalAngle, gimbalRotations::Tilt);
            break;
        } 
        case Manager::MOVE_GIMBAL_RIGHT:
        {
            gimbalAngle = (float)this->networkClient.receiveBuffer[++i];
            printf("GIMBAL RIGHT ANGLE: %f \n", gimbalAngle);
            this->gimbal.addAngle((float)(-gimbalAngle), gimbalRotations::Pan);
            break;
        } 
        case Manager::MOVE_GIMBAL_LEFT:
        {
            gimbalAngle = (float)this->networkClient.receiveBuffer[++i];
            printf("GIMBAL LEFT ANGLE: %f \n", gimbalAngle);
            this->gimbal.addAngle((float)gimbalAngle, gimbalRotations::Pan);
            break;
        }                            
        default:
            break;
        }
    }   

    sendData(responseBuffer, currentBufferIndex);

    this->networkClient.receivedFrameSize = 0;         
}

void Manager::appendBuffer(uint8_t *buffer, uint32_t &startIndex, uint8_t* newData, uint32_t newDataSize)
{
    for(int i = 0; i < newDataSize; i++)
    {
        buffer[startIndex++] = newData[i];        
    }
}