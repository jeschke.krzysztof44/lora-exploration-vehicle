/*
    Manager.h
    
    Author:     Krzysztof Jeschke
    Created:    31.05.2022
*/

#pragma once

#ifndef __Manager_H__
#define __Manager_H__

#include "NetworkClient.h"
#include "Gimbal.h"
#include "Camera.h"
#include "../../include/config.h"

#include <string.h>

class Manager
{

// variables
public:


    
private:

    static constexpr const char *TAG = "Manager";

    enum Commands
    {
        MY_ADDRESS = (uint8_t)1,
        
        TAKE_PHOTO = (uint8_t)40,
        MOVE_GIMBAL_UP = (uint8_t)41,
        MOVE_GIMBAL_DOWN = (uint8_t)42,
        MOVE_GIMBAL_RIGHT = (uint8_t)43,
        MOVE_GIMBAL_LEFT = (uint8_t)44,        

        ERROR = (uint8_t)99
    };      


    Gimbal gimbal;
    NetworkClient networkClient;
    Camera camera;
// methods
public:
    Manager();
    ~Manager();

    esp_err_t init();
    void run();

private:
    bool isNewData();
    void runReceivedCommands();
    void sendData(uint8_t data[], int dataSize);
    void receiveData();

    void appendBuffer(uint8_t *buffer, uint32_t &startIndex, uint8_t* newData, uint32_t newDataSize);
};

#endif