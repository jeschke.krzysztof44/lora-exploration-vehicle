/*
    Gimbal.h
    
    Author:     Krzysztof Jeschke
    Created:    22.03.2022
*/

#pragma once

#ifndef __Gimbal_H__
#define __Gimbal_H__

#include "../../include/config.h"
#include "iot_servo.h"


// Controls camera pan and tilt moves using two TowerPro MG995 servomotors connectd with each other.
class Gimbal
{

// variables
public:

private:
    static constexpr const char *TAG = "Gimbal";

    servo_config_t servo_config;
    const float panMaxAngle = 160;
    const float panMinAngle = 0;
    const float tiltMaxAngle = 160;
    const float tiltMinAngle = 70;

    float panCurrentAngle;
    float tiltCurrentAngle;

// methods
public:
    Gimbal();
    ~Gimbal();

    //Set new angle softly
    esp_err_t setAngle(float angle, gimbalRotations rotation);
    esp_err_t addAngle(float angle, gimbalRotations rotation);
    

    void init();
    void goToZero();

    float getCurrentAngle(gimbalRotations rotationType);

    esp_err_t checkNewAngle(float newAngle, gimbalRotations rotationType);

private:
    void setCurrentAngle(float newAngle, gimbalRotations rotationType);
    esp_err_t forceAngle(float newAngle, gimbalRotations rotationType);
};

#endif