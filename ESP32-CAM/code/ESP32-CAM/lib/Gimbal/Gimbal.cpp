/*
    Gimbal.cpp
    
    Author:     Krzysztof Jeschke
    Created:    22.03.2022
*/

#include "Gimbal.h"

Gimbal::Gimbal()
{
}

Gimbal::~Gimbal()
{
}

void Gimbal::init()
{
    servo_config = {
        .max_angle = 180,
        .min_width_us = 500,
        .max_width_us = 2500,
        .freq = 50,
        .timer_number = LEDC_TIMER_0,
        .channels = {
            .servo_pin = {
                SERVO_PAN_PIN,
                SERVO_TILT_PIN,
            },
            .ch = {
                LEDC_PAN_CHANNEL,
                LEDC_TILT_CHANNEL,
            },
        },
        .channel_number = 2,
    };

    iot_servo_init(LEDC_LOW_SPEED_MODE, &servo_config);

}

void Gimbal::goToZero()
{
    panCurrentAngle = 95.0f;
    forceAngle(panCurrentAngle, Pan);
    tiltCurrentAngle = 160.0f;
    forceAngle(tiltCurrentAngle, Tilt);

    vTaskDelay(2000 / portTICK_RATE_MS);
}

esp_err_t Gimbal::forceAngle(float newAngle, gimbalRotations rotationType)
{
    return iot_servo_write_angle(LEDC_LOW_SPEED_MODE, rotationType, newAngle);
}

esp_err_t Gimbal::setAngle(float newAngle, gimbalRotations rotationType)
{
    if (ESP_OK != checkNewAngle(newAngle, rotationType)) return ESP_FAIL;

    esp_err_t error_code;
    float currentAngle = getCurrentAngle(rotationType);
    if (currentAngle == newAngle) return ESP_OK;

    float sign = (currentAngle > newAngle) ? -1.0f : 1.0f;
    float angleDifference = abs(currentAngle - newAngle);
    
    for(float stepAngle = 0.0f; stepAngle <= angleDifference; stepAngle += 1.0f)
    {   
        error_code = iot_servo_write_angle(LEDC_LOW_SPEED_MODE, rotationType, currentAngle + (sign * stepAngle));
        if (error_code != ESP_OK)
        {
            return error_code;
        }
        vTaskDelay(15 / portTICK_RATE_MS);
    }

    setCurrentAngle(newAngle, rotationType);
    return ESP_OK;
}

esp_err_t Gimbal::addAngle(float angle, gimbalRotations rotation)
{
    return setAngle(this->getCurrentAngle(rotation) + angle, rotation);
}


esp_err_t Gimbal::checkNewAngle(float newAngle, gimbalRotations rotationType)
{
    switch(rotationType)
    {
        case Pan:
            if(newAngle < panMinAngle || newAngle > panMaxAngle)
            {
                ESP_LOGE(TAG, "New pan angle is not correct, value should be between: <%f, %f>.", panMinAngle, panMaxAngle);
                return ESP_FAIL;
            }
            break;
        case Tilt:
            if(newAngle < tiltMinAngle || newAngle > tiltMaxAngle)
            {
                ESP_LOGE(TAG, "New tilt angle is not correct, value should be between: <%f, %f>.", tiltMinAngle, tiltMaxAngle);
                return ESP_FAIL;
            }
            break;
    }

    return ESP_OK;
}

float Gimbal::getCurrentAngle(gimbalRotations rotationType)
{
    switch(rotationType)
    {
        case Pan:
            return panCurrentAngle;
            break;
        case Tilt:
            return tiltCurrentAngle;
            break;
    }
    ESP_LOGE(TAG, "getCurrentAngle: No such rotation type: %d", rotationType);
    return -1;
}

void Gimbal::setCurrentAngle(float newAngle, gimbalRotations rotationType)
{
    switch(rotationType)
    {
        case Pan:
            panCurrentAngle = newAngle;
            return;
        case Tilt:
            tiltCurrentAngle = newAngle;
            return;
    }

    ESP_LOGE(TAG, "setCurrentAngle: No such rotation type: %d", rotationType);
}