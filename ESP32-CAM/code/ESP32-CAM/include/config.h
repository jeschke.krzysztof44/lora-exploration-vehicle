#pragma once

#ifndef CONFIG_H_
#define CONFIG_H_

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "iot_servo.h"

#define CHECK(tag, ret, txt)  { esp_err_t __; if ((__ = ret) != ESP_OK){ ESP_LOGE(tag, "%s call failed, error code: %d", txt, ret); } }
#define sleep_ms(time_ms) (vTaskDelay(time_ms / portTICK_RATE_MS))

//timer settings
#define TIMER_DIVIDER               (80)                                //  Hardware timer clock divider
#define TIMER_SCALE                 (TIMER_BASE_CLK / TIMER_DIVIDER)    // convert counter value to seconds

//Gimbal servo motors - pan & tilt
#define SERVO_PAN_PIN               GPIO_NUM_13    
#define SERVO_TILT_PIN              GPIO_NUM_15
#define LEDC_PAN_CHANNEL            LEDC_CHANNEL_0  
#define LEDC_TILT_CHANNEL           LEDC_CHANNEL_1

// Camera pins
#define CAM_PIN_PWDN 32
#define CAM_PIN_RESET -1 //software reset will be performed
#define CAM_PIN_XCLK 0
#define CAM_PIN_SIOD 26
#define CAM_PIN_SIOC 27

#define CAM_PIN_D7 35
#define CAM_PIN_D6 34
#define CAM_PIN_D5 39
#define CAM_PIN_D4 36
#define CAM_PIN_D3 21
#define CAM_PIN_D2 19
#define CAM_PIN_D1 18
#define CAM_PIN_D0 5
#define CAM_PIN_VSYNC 25
#define CAM_PIN_HREF 23
#define CAM_PIN_PCLK 22

//TCP Client
#define TCP_RECEIVE_BUFFER_SIZE 64
#define TCP_RESPONSE_BUFFER_SIZE 64

enum gimbalRotations
{
    Pan = LEDC_PAN_CHANNEL,
    Tilt = LEDC_TILT_CHANNEL,
};

#endif