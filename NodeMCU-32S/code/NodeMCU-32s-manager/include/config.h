#pragma once

#ifndef CONFIG_H_
#define CONFIG_H_

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "cstring"

#define CHECK(tag, ret, txt)  { esp_err_t __; if ((__ = ret) != ESP_OK){ ESP_LOGE(tag, "%s call failed, error code: %d", txt, ret); } }
#define CHECK_OK(tag, ret, txt)  ({ bool retVal=true; esp_err_t __; if ((__ = ret) != ESP_OK){ ESP_LOGE(tag, "%s call failed, error code: %d", txt, ret); retVal=false;} retVal;})
#define CHECK_ABORT(tag, ret, txt) { esp_err_t __; if ((__ = ret) != ESP_OK){ ESP_LOGE(tag, "%s call failed, error code: %d", txt, ret); abort(); }}
#define sleep_us(time_us) (vTaskDelay(((float)time_us / 1000) / portTICK_RATE_MS))
#define sleep_ms(time_ms) (vTaskDelay(time_ms / portTICK_RATE_MS))
#define sleep_s(time_s) (vTaskDelay((time_s * 1000) / portTICK_RATE_MS))

// TCP
#define TCP_ACTIONS_BUFFER_SIZE     256
#define TCP_CAMERA_BUFFER_SIZE      90000
#define TCP_RESPONSE_BUFFER_SIZE    (TCP_CAMERA_BUFFER_SIZE + TCP_ACTIONS_BUFFER_SIZE)

// legacy??
#define CLIENTS_NUM                 2

//I2C
#define I2C_SDA_PIN GPIO_NUM_21
#define I2C_SCL_PIN GPIO_NUM_22
#define I2C_FREQ_HZ                 100000
#define I2C_MASTER_NUM I2C_NUM_0

#define I2C_SHT30_ADDRESS           0x44
#define I2C_BMP180_ADDRESS          0x77
#define I2C_TSL2591_ADDRESS         0x29

// I2C common protocol defines
#define WRITE_BIT                   I2C_MASTER_WRITE    /*!< I2C master write */
#define READ_BIT                    I2C_MASTER_READ     /*!< I2C master read */
#define ACK_CHECK_EN                0x1                 /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS               0x0                 /*!< I2C master will not check ack from slave */
#define ACK_VAL                     I2C_MASTER_ACK      /*!< I2C ack value */
#define NACK_VAL                    I2C_MASTER_NACK     /*!< I2C nack value */
#define I2C_TX_BUF_DISABLE          0                   /*!< I2C master do not need buffer */
#define I2C_RX_BUF_DISABLE          0                   /*!< I2C master do not need buffer */

//timer settings
#define TIMER_DIVIDER               (80)                                //  Hardware timer clock divider
#define TIMER_SCALE                 (TIMER_BASE_CLK / TIMER_DIVIDER)    // convert counter value to seconds

// Ultrasonic sensors
#define HCSR04_TRIG_PIN             GPIO_NUM_16
#define HCSR04_FRONT_ECHO_PIN       GPIO_NUM_19      
#define HCSR04_RIGHT_ECHO_PIN       GPIO_NUM_18
#define HCSR04_BACK_ECHO_PIN        GPIO_NUM_5
#define HCSR04_LEFT_ECHO_PIN        GPIO_NUM_17

// DC motors
#define FRONT_LEFT_MOTOR_IN1_PIN    GPIO_NUM_32
#define FRONT_LEFT_MOTOR_IN2_PIN    GPIO_NUM_33
#define BACK_LEFT_MOTOR_IN1_PIN     GPIO_NUM_25
#define BACK_LEFT_MOTOR_IN2_PIN     GPIO_NUM_26
#define FRONT_RIGHT_MOTOR_IN1_PIN   GPIO_NUM_27
#define FRONT_RIGHT_MOTOR_IN2_PIN   GPIO_NUM_12
#define BACK_RIGHT_MOTOR_IN1_PIN    GPIO_NUM_4
#define BACK_RIGHT_MOTOR_IN2_PIN    GPIO_NUM_2

#define MOTOR_POWER 70.0f

enum MotorPosition
{
    FRONT_LEFT = 0,
    BACK_LEFT,
    FRONT_RIGHT,
    BACK_RIGHT,
};

enum UltrasonicDevPosition
{
    FRONT,
    RIGHT,
    BACK,
    LEFT
};

#endif