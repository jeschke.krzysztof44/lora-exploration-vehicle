/*
    SHT30.h
    
    Author:     Krzysztof Jeschke
    Created:    10.04.2022
*/

#pragma once

#ifndef __SHT30_H__
#define __SHT30_H__

#include "../../include/config.h"

#define CLK_STREACHING_EN                           (uint8_t) 0x2c
#define CLK_STREACHING_DIS                          (uint8_t) 0x24

#define REPEATABILITY_HIGH_CLK_STREACHING_EN        (uint16_t) (((uint16_t)CLK_STREACHING_EN << 8) | (uint8_t) 0x06)
#define REPEATABILITY_MEDIUM_CLK_STREACHING_EN      (uint16_t) (((uint16_t)CLK_STREACHING_EN << 8) | (uint8_t) 0x0d)
#define REPEATABILITY_LOW_CLK_STREACHING_EN         (uint16_t) (((uint16_t)CLK_STREACHING_EN << 8) | (uint8_t) 0x10)

#define REPEATABILITY_HIGH_CLK_STREACHING_DIS       (uint16_t) (((uint16_t)CLK_STREACHING_DIS << 8) | (uint8_t) 0x00)
#define REPEATABILITY_MEDIUM_CLK_STREACHING_DIS     (uint16_t) (((uint16_t)CLK_STREACHING_DIS << 8) | (uint8_t) 0x0b)
#define REPEATABILITY_LOW_CLK_STREACHING_DIS        (uint16_t) (((uint16_t)CLK_STREACHING_DIS << 8) | (uint8_t) 0x16)

#define MEASUREMENT_COMMAND                         (uint16_t) REPEATABILITY_HIGH_CLK_STREACHING_DIS
#define MAX_READ_NUM                                (uint8_t) 10
#define POLYNOMIAL                                  (uint8_t) 0x31

class SHT30
{

// variables
public:

private:
    static constexpr const char *TAG = "SHT30";

    uint16_t temperatureRaw;
    uint16_t humidityRaw;
    float temperature;
    float humidity;

// methods
public:
    SHT30();
    ~SHT30();

    esp_err_t init();

    float getTemperature();
    float getHumidity();
    esp_err_t updateMeasuredData();

private:
    esp_err_t startMeasurement();
    esp_err_t readMeasuredData();
    uint8_t crc8(uint8_t MSB, uint8_t LSB);

};

#endif