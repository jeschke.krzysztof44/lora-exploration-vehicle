/*
    MotorDriver.cpp
    
    Author:     Krzysztof Jeschke
    Created:    31.05.2022
*/

#include "MotorDriver.h"

MotorDriver::MotorDriver()
{
}

MotorDriver::~MotorDriver()
{
}

esp_err_t MotorDriver::init()
{
    esp_err_t errorCode;

    initMotors();

    for(Motor &motor : motors)
    {
        errorCode = mcpwm_gpio_init(motor.mcpwmUnitNum, motor.mcpwmIoSignalA, motor.IN1Pin);
        if(errorCode != ESP_OK) return errorCode;

        errorCode = mcpwm_gpio_init(motor.mcpwmUnitNum, motor.mcpwmIoSignalB, motor.IN2Pin);
        if(errorCode != ESP_OK) return errorCode;
    }


    return ESP_OK;
}

void MotorDriver::initMotors()
{
    // FRONT_LEFT
    this->motors[0].IN1Pin = FRONT_LEFT_MOTOR_IN1_PIN;
    this->motors[0].IN2Pin = FRONT_LEFT_MOTOR_IN2_PIN;
    this->motors[0].mcpwmUnitNum = MCPWM_UNIT_0;
    this->motors[0].mcpwmIoSignalA = MCPWM0A;
    this->motors[0].mcpwmIoSignalB = MCPWM0B;
    this->motors[0].timerNum = MCPWM_TIMER_0;

    // BACK_LEFT
    this->motors[1].IN1Pin = BACK_LEFT_MOTOR_IN1_PIN;
    this->motors[1].IN2Pin = BACK_LEFT_MOTOR_IN2_PIN;
    this->motors[1].mcpwmUnitNum = MCPWM_UNIT_0;
    this->motors[1].mcpwmIoSignalA = MCPWM1A;
    this->motors[1].mcpwmIoSignalB = MCPWM1B;    
    this->motors[1].timerNum = MCPWM_TIMER_1;

    // FRONT_RIGHT
    this->motors[2].IN1Pin = FRONT_RIGHT_MOTOR_IN1_PIN;
    this->motors[2].IN2Pin = FRONT_RIGHT_MOTOR_IN2_PIN;
    this->motors[2].mcpwmUnitNum = MCPWM_UNIT_0;
    this->motors[2].mcpwmIoSignalA = MCPWM2A;
    this->motors[2].mcpwmIoSignalB = MCPWM2B;    
    this->motors[2].timerNum = MCPWM_TIMER_2;

    // BACK_RIGHT
    this->motors[3].IN1Pin = BACK_RIGHT_MOTOR_IN1_PIN;
    this->motors[3].IN2Pin = BACK_RIGHT_MOTOR_IN2_PIN;
    this->motors[3].mcpwmUnitNum = MCPWM_UNIT_1;
    this->motors[3].mcpwmIoSignalA = MCPWM0A;
    this->motors[3].mcpwmIoSignalB = MCPWM0B;    
    this->motors[3].timerNum = MCPWM_TIMER_0;        

    mcpwm_config_t pwm_config;
    pwm_config.frequency = 1000;    //frequency = 500Hz,
    pwm_config.cmpr_a = 0;          //duty cycle of PWMxA = 0
    pwm_config.cmpr_b = 0;          //duty cycle of PWMxb = 0
    pwm_config.counter_mode = MCPWM_UP_COUNTER;
    pwm_config.duty_mode = MCPWM_DUTY_MODE_0;
    
    mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config);
    mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_1, &pwm_config);
    mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_2, &pwm_config);
    mcpwm_init(MCPWM_UNIT_1, MCPWM_TIMER_0, &pwm_config);

}

void MotorDriver::motorForward(Motor motor, float duty_cycle)
{
    mcpwm_set_signal_low(motor.mcpwmUnitNum, motor.timerNum, MCPWM_GEN_B);
    mcpwm_set_duty(motor.mcpwmUnitNum, motor.timerNum, MCPWM_GEN_A, duty_cycle);
    mcpwm_set_duty_type(motor.mcpwmUnitNum, motor.timerNum, MCPWM_GEN_A, MCPWM_DUTY_MODE_0); //call this each time, if operator was previously in low/high state
}

void MotorDriver::motorBackward(Motor motor, float duty_cycle)
{
    mcpwm_set_signal_low(motor.mcpwmUnitNum, motor.timerNum, MCPWM_GEN_A);
    mcpwm_set_duty(motor.mcpwmUnitNum, motor.timerNum, MCPWM_GEN_B, duty_cycle);
    mcpwm_set_duty_type(motor.mcpwmUnitNum, motor.timerNum, MCPWM_GEN_B, MCPWM_DUTY_MODE_0);  //call this each time, if operator was previously in low/high state
}

void MotorDriver::motorStop(Motor motor)
{
    mcpwm_set_signal_low(motor.mcpwmUnitNum, motor.timerNum, MCPWM_GEN_A);
    mcpwm_set_signal_low(motor.mcpwmUnitNum, motor.timerNum, MCPWM_GEN_B);
}

void MotorDriver::vehicleForward(float speed)
{
    motorForward(this->motors[FRONT_LEFT], speed);
    motorForward(this->motors[BACK_LEFT], speed);
    motorForward(this->motors[FRONT_RIGHT], speed);
    motorForward(this->motors[BACK_RIGHT], speed);

}

void MotorDriver::vehicleBackward(float speed)
{
    motorBackward(this->motors[FRONT_LEFT], speed);
    motorBackward(this->motors[BACK_LEFT], speed);
    motorBackward(this->motors[FRONT_RIGHT], speed);
    motorBackward(this->motors[BACK_RIGHT], speed);
}

void MotorDriver::vehicleRight(float speed)
{
    motorBackward(this->motors[FRONT_LEFT], speed);
    motorForward(this->motors[BACK_LEFT], speed);
    motorForward(this->motors[FRONT_RIGHT], speed);
    motorBackward(this->motors[BACK_RIGHT], speed);
}

void MotorDriver::vehicleLeft(float speed)
{
    motorForward(this->motors[FRONT_LEFT], speed);
    motorBackward(this->motors[BACK_LEFT], speed);
    motorBackward(this->motors[FRONT_RIGHT], speed);
    motorForward(this->motors[BACK_RIGHT], speed);
}
void MotorDriver::vehicleForwardRight(float speed)
{
    motorForward(this->motors[BACK_LEFT], speed);
    motorForward(this->motors[FRONT_RIGHT], speed);
}
void MotorDriver::vehicleForwardLeft(float speed)
{
    motorForward(this->motors[FRONT_LEFT], speed);
    motorForward(this->motors[BACK_RIGHT], speed);
}
void MotorDriver::vehicleBackwardRight(float speed)
{
    motorBackward(this->motors[FRONT_LEFT], speed);
    motorBackward(this->motors[BACK_RIGHT], speed);
}
void MotorDriver::vehicleBackwardLeft(float speed)
{
    motorBackward(this->motors[BACK_LEFT], speed);
    motorBackward(this->motors[FRONT_RIGHT], speed);
}
void MotorDriver::vehicleTurnAroundLeft(float speed)
{
    motorBackward(this->motors[FRONT_LEFT], speed);
    motorBackward(this->motors[BACK_LEFT], speed);
    motorForward(this->motors[FRONT_RIGHT], speed);
    motorForward(this->motors[BACK_RIGHT], speed);    
}
void MotorDriver::vehicleTurnAroundRight(float speed)
{
    motorForward(this->motors[FRONT_LEFT], speed);
    motorForward(this->motors[BACK_LEFT], speed);
    motorBackward(this->motors[FRONT_RIGHT], speed);
    motorBackward(this->motors[BACK_RIGHT], speed);   
}

void MotorDriver::vehicleStop()
{
    motorStop(this->motors[FRONT_LEFT]);
    motorStop(this->motors[BACK_LEFT]);
    motorStop(this->motors[FRONT_RIGHT]);
    motorStop(this->motors[BACK_RIGHT]);
}