/*
    MotorDriver.h
    
    Author:     Krzysztof Jeschke
    Created:    10.04.2022
*/

#pragma once

#ifndef __MotorDriver_H__
#define __MotorDriver_H__

#include "driver/mcpwm.h"
#include "soc/mcpwm_reg.h"
#include "soc/mcpwm_struct.h"
#include "../../include/config.h"

#define MOTORS_NUM  (uint8_t)4

class MotorDriver
{

struct Motor
{
    gpio_num_t IN1Pin;
    gpio_num_t IN2Pin;
    mcpwm_unit_t mcpwmUnitNum;
    mcpwm_io_signals_t mcpwmIoSignalA;
    mcpwm_io_signals_t mcpwmIoSignalB;
    mcpwm_timer_t timerNum;
};

// variables
public:
    Motor motors[MOTORS_NUM];

private:
    static constexpr const char *TAG = "MotorDriver";
    int test = MCPWM_OPR_B;

// methods
public:
    MotorDriver();
    ~MotorDriver();

    esp_err_t init();
    void initMotors();

    void motorForward(Motor motor, float duty_cycle);
    void motorBackward(Motor motor, float duty_cycle);
    void motorStop(Motor motor);

    void vehicleForward(float speed);
    void vehicleBackward(float speed);
    void vehicleRight(float speed);
    void vehicleLeft(float speed);
    void vehicleForwardRight(float speed);
    void vehicleForwardLeft(float speed);
    void vehicleBackwardRight(float speed);
    void vehicleBackwardLeft(float speed);
    void vehicleTurnAroundLeft(float speed);
    void vehicleTurnAroundRight(float speed);

    void vehicleStop();


private:


};

#endif