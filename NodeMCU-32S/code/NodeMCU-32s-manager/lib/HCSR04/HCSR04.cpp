/*
    HCSR04.cpp
    
    Author:     Krzysztof Jeschke
    Created:    25.04.2022
*/

#include "HCSR04.h"

HCSR04::HCSR04(gpio_num_t trigPin, gpio_num_t echoPin)
{
    this->trigPin = trigPin;
    this->echoPin = echoPin;
}

HCSR04::~HCSR04()
{
}

esp_err_t HCSR04::init()
{
    gpio_set_direction(this->trigPin, GPIO_MODE_OUTPUT);
    gpio_set_direction(this->echoPin, GPIO_MODE_INPUT);

    return gpio_set_level(this->trigPin, 0);
}

esp_err_t HCSR04::updateMeasuredData()
{
    // Ping: Low for 2..4 us, then high 10 us
    gpio_set_level(this->trigPin, 0);
    sleep_us(TRIG_LOW_US);
    gpio_set_level(this->trigPin, 1);
    sleep_us(TRIG_HIGH_US);
    gpio_set_level(this->trigPin, 0);

    // Previous ping isn't ended
    if (gpio_get_level(this->echoPin))
    {
        printf("Previous ping isn't ended\n");
        this->currentMeasureError = ESP_FAIL;
        return this->currentMeasureError;
    }

    // Wait for echo
    int64_t start = esp_timer_get_time();
    while (!gpio_get_level(this->echoPin))
    {
        if (timeout_expired(start, PING_TIMEOUT))
        {
            printf("Wait for echo expired\n");
            // printf("TIME %f\n", ((float)TRIG_LOW_US/1000));
            this->currentMeasureError = ESP_ERR_TIMEOUT;
            return this->currentMeasureError;
        }
    }

    // got echo, measuring
    int64_t echoStart = esp_timer_get_time();
    int64_t time = echoStart;
    while (gpio_get_level(this->echoPin))
    {
        time = esp_timer_get_time();
        if (timeout_expired(echoStart, MAX_ECHO_TIME_US))
        {
            printf("Got echo expired\n");
            this->currentMeasureError = ESP_ERR_TIMEOUT;
            return this->currentMeasureError;
        }
    }
    // printf("TIME: %lld\n", time);
    // printf("ECHO START: %lld\n", echoStart);
    this->echoTime = time - echoStart;

    this->currentMeasureError = ESP_OK;
    return this->currentMeasureError;
}

uint32_t HCSR04::getDistanceCm()
{
    if (this->currentMeasureError != ESP_OK)
    {
        return -1;
    }
    
    return this->echoTime / ROUNDTRIP_CM;
}