/*
    HCSR04.h
    
    Author:     Krzysztof Jeschke
    Created:    25.04.2022
*/

#pragma once

#ifndef __HCSR04_H__
#define __HCSR04_H__

#define TRIG_LOW_US         (uint8_t)4
#define TRIG_HIGH_US        (uint8_t)10
#define PING_TIMEOUT        (uint16_t)12000
#define ROUNDTRIP_CM        (uint32_t)58
#define MAX_DISTANCE_M      (float)2.1
#define INITIAL_SOUND_SPEED (float)340
#define MAX_ECHO_TIME_US    (float)((MAX_DISTANCE_M / INITIAL_SOUND_SPEED) * 1000000.0f * 2.0f)

#define timeout_expired(start, len) ((esp_timer_get_time() - (start)) >= (len))

#include "../../include/config.h"


class HCSR04
{

// variables
public:
    int64_t echoTime = -1;
private:
    static constexpr const char *TAG = "HCSR04";
    gpio_num_t trigPin;
    gpio_num_t echoPin;
    esp_err_t currentMeasureError = ESP_OK;

    
// methods
public:
    HCSR04(gpio_num_t trigPin, gpio_num_t echoPin);
    ~HCSR04();

    esp_err_t init();
    esp_err_t updateMeasuredData();
    uint32_t getDistanceCm();
private:

};

#endif