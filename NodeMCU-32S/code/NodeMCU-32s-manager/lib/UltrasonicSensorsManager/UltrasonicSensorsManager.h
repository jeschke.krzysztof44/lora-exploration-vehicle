/*
    UltrasonicSensorsManager.h
    
    Author:     Krzysztof Jeschke
    Created:    25.04.2022
*/

#pragma once

#ifndef __UltrasonicSensorsManager_H__
#define __UltrasonicSensorsManager_H__

#define TRIG_LOW_US         (uint8_t)4
#define TRIG_HIGH_US        (uint8_t)10
#define PING_TIMEOUT        (uint16_t)12000
#define ROUNDTRIP_CM        (uint32_t)58
#define MAX_DISTANCE_M      (float)2.1
#define INITIAL_SOUND_SPEED (float)340
#define MAX_ECHO_TIME_US    (float)((MAX_DISTANCE_M / INITIAL_SOUND_SPEED) * 1000000.0f * 2.0f)

#define timeout_expired(start, len) ((esp_timer_get_time() - (start)) >= (len))

#include "../../include/config.h"
#include "HCSR04.h"
#include <map>


class UltrasonicSensorsManager
{

// variables
public:
private:
    static constexpr const char *TAG = "UltrasonicSensorsManager";

    std::map<uint8_t, HCSR04> ultrasonicDevices {   {FRONT, HCSR04{HCSR04_TRIG_PIN, HCSR04_FRONT_ECHO_PIN}}, 
                                                    {RIGHT, HCSR04{HCSR04_TRIG_PIN, HCSR04_RIGHT_ECHO_PIN}},
                                                    {BACK, HCSR04{HCSR04_TRIG_PIN, HCSR04_BACK_ECHO_PIN}},
                                                    {LEFT, HCSR04{HCSR04_TRIG_PIN, HCSR04_LEFT_ECHO_PIN}}
                                                };
                                                  
// methods
public:
    UltrasonicSensorsManager();
    ~UltrasonicSensorsManager();

    esp_err_t init();
    esp_err_t updateAllSensorsData();
    uint32_t getDistanceCm(UltrasonicDevPosition devPosition);

private:

};

#endif