/*
    NetworkClient.cpp
    
    Author:     Krzysztof Jeschke
    Created:    31.05.2022
*/

#include "NetworkClient.h"

NetworkClient::NetworkClient()
{

}

NetworkClient::~NetworkClient()
{
}

// esp_err_t NetworkClient::init(TcpData &tcpData)
// {
//     this->tcpData = &tcpData;
//     //Initialize NVS
//     esp_err_t ret = nvs_flash_init();
//     if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
//       ESP_ERROR_CHECK(nvs_flash_erase());
//       ret = nvs_flash_init();
//     }
//     ESP_ERROR_CHECK(ret);

//     s_retry_num = 0;

//     return ret;
// }

esp_err_t NetworkClient::init()
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    s_retry_num = 0;

    return ret;
}

void NetworkClient::event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < ESP_MAXIMUM_RETRY) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}


void NetworkClient::wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_got_ip));

    wifi_config_t wifi_config = { .sta =  {
        /* ssid            */ ESP_WIFI_SSID,
        /* password        */ ESP_WIFI_PASS,
        /* scan_method     */ {},
        /* bssid_set       */ {},
        /* bssid           */ {},
        /* channel         */ 1,
        /* listen_interval */ {},
        /* sort_method     */ {},
        /* threshold       */ {
            /* rssi            */ {},
            /* authmode        */ ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD},
        /* pmf_cfg         */ {
            /* capable         */ true,
            /* required        */ false},
        /* rm_enabled      */ {},
        /* btm_enabled     */ {},
//      /* mbo_enabled     */ {}, // For IDF 4.4 and higher
        /* reserved        */ {}
    }};
    
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                 ESP_WIFI_SSID, ESP_WIFI_PASS);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                 ESP_WIFI_SSID, ESP_WIFI_PASS);
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }
}

void NetworkClient::createSocket()
{
    char host_ip[] = HOST_IP_ADDR;
    int addr_family = 0;
    int ip_protocol = 0;

    struct sockaddr_in dest_addr;
    dest_addr.sin_addr.s_addr = inet_addr(host_ip);
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(PORT);
    addr_family = AF_INET;
    ip_protocol = IPPROTO_IP;

    bool success = false;

    while (!success) 
    {
        this->currentSocket =  socket(addr_family, SOCK_STREAM, ip_protocol);
        if (this->currentSocket < 0) {
            ESP_LOGE(TAG, "Unable to create socket: errno %d", errno);
            continue;
        }
        ESP_LOGI(TAG, "Socket created, connecting to %s:%d", host_ip, PORT);

        int err = connect(this->currentSocket, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_in6));
        if (err != 0) {
            ESP_LOGE(TAG, "Socket unable to connect: errno %d", errno);
            continue;
        }
        ESP_LOGI(TAG, "Successfully connected"); 
        success = true;   
    }
}

void NetworkClient::closeSocket()
{
    ESP_LOGE(TAG, "Shutting down socket and restarting...");
    shutdown(this->currentSocket, 0);
    close(this->currentSocket);
}

void NetworkClient::reconnect()
{
    closeSocket();
    createSocket();
}

esp_err_t NetworkClient::receiveData()
{
    // const std::lock_guard<std::mutex> lock(this->tcpData->tcp_buffer_mutex);
    this->receivedFrameSize = recv(this->currentSocket, this->receiveBuffer, sizeof(this->receiveBuffer) - 1, 0);
    if (this->receivedFrameSize < 0) {
        ESP_LOGE(TAG, "recv failed: errno %d", errno);
        return ESP_FAIL;
    }

    ESP_LOGI(TAG, "Received %d bytes.", this->receivedFrameSize);
    return ESP_OK;
}

esp_err_t NetworkClient::sendData(char data[], uint8_t dataSize)
{
    int err = send(this->currentSocket, data, dataSize, 0);
    if (err < 0) {
        ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
        return ESP_FAIL;
    }
    
    ESP_LOGI(TAG, "Sent %d bytes.", dataSize);
    return ESP_OK;
}
