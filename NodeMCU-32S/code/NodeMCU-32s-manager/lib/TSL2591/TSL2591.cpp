/*
    TSL2591.cpp
    
    Author:     Krzysztof Jeschke
    Created:    10.04.2022
*/

#include "TSL2591.h"

TSL2591::TSL2591()
{
}

TSL2591::~TSL2591()
{
}

esp_err_t TSL2591::init()
{
    esp_err_t ret;
    ret = setControlRegister(INTEGRATIONTIME_300MS, GAIN_MED);
    ret = enable();

    return ret;
}

esp_err_t TSL2591::enable()
{
    return writeRegister(REG_ENABLE, ENABLE_POWER_ON | ENABLE_AEN | ENABLE_AIEN | ENABLE_NPIEN);
}

esp_err_t TSL2591::setControlRegister(integrationTime_t integrationTimeSetting, gain_t gainSetting)
{
    esp_err_t ret = writeRegister(REG_CONTROL, integrationTimeSetting| gainSetting);

    if (CHECK_OK(TAG, ret, "Set control register"))
    {
        this->currentIntegrationTimeSetting = integrationTimeSetting;
        this->currentGainSetting = gainSetting;

        this->currentIntegrationTime = (float)(integrationTimeSetting + 1) * 100.0f;
        this->currentGain = gainSettingToGain(gainSetting);
    }

    return ret;
}

float TSL2591::gainSettingToGain(gain_t gainSetting)
{
    switch (gainSetting)
    {
    case GAIN_LOW:
        return 1.0f;
        break;
    case GAIN_MED:
        return 25.0f;
        break;
    case GAIN_HIGH:
        return 428.0f;
        break;
    case GAIN_MAX:
        return 9876.0f;
        break;
    default:
        return 1.0f;
    }
}

esp_err_t TSL2591::writeRegister(uint8_t regAddr, uint8_t regData)
{
    esp_err_t ret;

    for (int i = 0; i < MAX_READ_NUM; i++)
    {
        i2c_cmd_handle_t cmd = i2c_cmd_link_create();

        // send register address
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (I2C_TSL2591_ADDRESS << 1) | WRITE_BIT, ACK_CHECK_EN);
        i2c_master_write_byte(cmd, REG_COMMAND | TRANSACTION_NORMAL | regAddr, ACK_CHECK_EN);

        // send data to register
        i2c_master_write_byte(cmd, regData, ACK_CHECK_EN);
        i2c_master_stop(cmd);

        ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
        i2c_cmd_link_delete(cmd);

        if (CHECK_OK(TAG, ret, "Write register"))
        {
            return ret;
        }
        ESP_LOGE(TAG, "Read data try num %i failed, trying again.", i);

        vTaskDelay(5 / portTICK_RATE_MS);
    }
    return ret; 
}

esp_err_t TSL2591::updateMeasuredData()
{
    const uint8_t dataLenght = 2;
    uint8_t buffer[dataLenght] = {0,0};

    esp_err_t ret = readRegisters(REG_C0DATAL, buffer, dataLenght);
    if (CHECK_OK(TAG, ret, "CH0 read"))
    {
        this->lightIntensityRawCh0 = (uint16_t)buffer[1] << 8 | buffer[0];

        ret = readRegisters(REG_C1DATAL, buffer, dataLenght);
        if (CHECK_OK(TAG, ret, "CH1 read"))
        {
            this->lightIntensityRawCh1 = (uint16_t)buffer[1] << 8 | buffer[0];
            this->lux = convertToLux();
        }
    }




    return ret;    
}

esp_err_t TSL2591::readRegisters(uint8_t firstRegAddr, uint8_t *readData, size_t dataLenght)
{
    esp_err_t ret;
    
    for (int i = 0; i < MAX_READ_NUM; i++)
    {
        i2c_cmd_handle_t cmd = i2c_cmd_link_create();

        // send register address
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (I2C_TSL2591_ADDRESS << 1) | WRITE_BIT, ACK_CHECK_EN);
        i2c_master_write_byte(cmd, REG_COMMAND | TRANSACTION_NORMAL | firstRegAddr, ACK_CHECK_EN);

        // read register address
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (I2C_TSL2591_ADDRESS << 1) | READ_BIT, ACK_CHECK_EN);
        i2c_master_read(cmd, readData, dataLenght, I2C_MASTER_LAST_NACK);
        i2c_master_stop(cmd);

        ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
        i2c_cmd_link_delete(cmd);

        if (CHECK_OK(TAG, ret, "Read registers"))
        {
            return ret;
        }
        ESP_LOGE(TAG, "Read data try num %i failed, trying again.", i);

        vTaskDelay(5 / portTICK_RATE_MS);
    }
    return ret;    
}

uint16_t TSL2591::getLightIntensityRaw(uint8_t channelNum)
{
    switch(channelNum)
    {
        case 0:
        {
            return this->lightIntensityRawCh0;
        }
        case 1:
        {
            return this->lightIntensityRawCh1;
        }
        default:
        {
            ESP_LOGE(TAG, "Incorrect channel number: %i", channelNum);
            return -1;
        }
    }
}

float TSL2591::getLux()
{
    return this->lux;
}

float TSL2591::convertToLux()
{
    float cpl, lux;

    cpl = (this->currentIntegrationTime * this->currentGain) / LUX_DF;

    lux = (((float)lightIntensityRawCh0 - (float)lightIntensityRawCh1)) * (1.0F - ((float)lightIntensityRawCh1 / (float)lightIntensityRawCh0)) / cpl;

    return lux;
}