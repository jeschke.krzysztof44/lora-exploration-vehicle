/*
    TSL2591.h
    
    Author:     Krzysztof Jeschke
    Created:    10.04.2022
*/

#pragma once

#ifndef __TSL2591_H__
#define __TSL2591_H__

#include "../../include/config.h"

#define REG_COMMAND         (uint8_t) 0b10000000    // Specifies Register Address
#define TRANSACTION_NORMAL  (uint8_t) 0b00100000    // Normal transaction for addressing registers

#define REG_ENABLE          (uint8_t) 0x00          // Enables states and interrupts
#define REG_CONTROL         (uint8_t) 0x01          // ALS gain and integration time configuration
#define REG_C0DATAL         (uint8_t) 0x14          // CH0 ADC low data byte
#define REG_C0DATAH         (uint8_t) 0x15          // CH0 ADC high data byte
#define REG_C1DATAL         (uint8_t) 0x16          // CH1 ADC low data byte
#define REG_C1DATAH         (uint8_t) 0x17          // CH1 ADC high data byte

// Enable register options
#define ENABLE_POWER_ON     (uint8_t) 0x01
#define ENABLE_AEN          (uint8_t) 0x02
#define ENABLE_AIEN         (uint8_t) 0x10
#define ENABLE_NPIEN        (uint8_t) 0x80

// lux caclulation parameters
#define LUX_DF              408.0F                  ///< Lux cooefficient
#define LUX_COEFB           1.64F                   ///< CH0 coefficient
#define LUX_COEFC           0.59F                   ///< CH1 coefficient A
#define LUX_COEFD           0.86F                   ///< CH2 coefficient B

#define MAX_READ_NUM        (uint8_t) 10

class TSL2591
{

// variables
public:

private:
    static constexpr const char *TAG = "TSL2591";
    float lux = 0.0f;

    uint16_t lightIntensityRawCh0 = 0;
    uint16_t lightIntensityRawCh1 = 0;

    // gain of the internal integration amplifiers for both photodiode channels
    typedef enum 
    {
        INTEGRATIONTIME_100MS = 0x00, // 100 millis
        INTEGRATIONTIME_200MS = 0x01, // 200 millis
        INTEGRATIONTIME_300MS = 0x02, // 300 millis
        INTEGRATIONTIME_400MS = 0x03, // 400 millis
        INTEGRATIONTIME_500MS = 0x04, // 500 millis
        INTEGRATIONTIME_600MS = 0x05, // 600 millis
    } integrationTime_t;

    // internal ADC integration time for both photodiode channels
    typedef enum 
    {
        GAIN_LOW = 0x00,  /// low gain (1x)
        GAIN_MED = 0x10,  /// medium gain (25x)
        GAIN_HIGH = 0x20, /// medium gain (428x)
        GAIN_MAX = 0x30,  /// max gain (9876x)
    } gain_t;

    integrationTime_t currentIntegrationTimeSetting;
    gain_t currentGainSetting;

    float currentIntegrationTime;
    float currentGain;

// methods
public:
    TSL2591();
    ~TSL2591();

    esp_err_t init();
    esp_err_t updateMeasuredData();

    float getLux();
    uint16_t getLightIntensityRaw(uint8_t channelNum);


private:
    esp_err_t setControlRegister(integrationTime_t integrationTimeSetting, gain_t gainSetting);
    esp_err_t enable();

    esp_err_t readRegisters(uint8_t firstRegAddr, uint8_t *outData, size_t dataLenght);
    esp_err_t writeRegister(uint8_t regAddr, uint8_t regData);

    float convertToLux();
    float gainSettingToGain(gain_t gainSetting);
};

#endif