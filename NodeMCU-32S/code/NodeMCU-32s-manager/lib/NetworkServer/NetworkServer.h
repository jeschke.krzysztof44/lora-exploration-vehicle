/*
    NetworkServer.h
    
    Author:     Krzysztof Jeschke
    Created:    25.04.2022
*/

#pragma once

#ifndef __NetworkServer_H__
#define __NetworkServer_H__

#include <string.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include "lwip/sockets.h"
#include <lwip/netdb.h>

#include <map>


#include "../../include/config.h"
//#include "../../include/node_mcu_private.h"

// AP
#define EXAMPLE_ESP_WIFI_SSID      "wifi_server"
#define EXAMPLE_ESP_WIFI_PASS      "123456789"
#define EXAMPLE_ESP_WIFI_CHANNEL   1
#define EXAMPLE_MAX_STA_CONN       2

// TCP Server
#define IP_ADDRESS                  "192.168.4.1"
#define PORT                        10001
#define KEEPALIVE_IDLE              5
#define KEEPALIVE_INTERVAL          5
#define KEEPALIVE_COUNT             3

class NetworkServer
{

// variables
public:
    //uint8_t receiveBuffer[TCP_BUFFER_SIZE];
    //int32_t receivedFrameSize = 0;
    std::map<uint8_t, uint8_t> deviceAddrAndSocket;

    union
    {
        int intData;
        uint8_t uint8_t_Data[sizeof(int)];
    } int_uint8_t_arr;  

private:
    static constexpr const char *TAG = "NetworkServer";
    int listenSocket;
    int sockets[CLIENTS_NUM];

    // <device_address, socket_number>
    
    // char send_buffer[TCP_BUFFER_SIZE];
    // uint8_t sent_frame_size = 0;

    bool enableSending = false;
    bool enableReceiving = false;

    // TcpData *tcpData;

// methods
public:
    NetworkServer();
    ~NetworkServer();

    esp_err_t init();
    void wifi_init_softap();
    void waitForClients();
    void tcp_server_task();
    
    int32_t receiveData(uint8_t buffer[], uint32_t bufferSize, uint8_t address);
    int receiveImage(uint8_t buffer[], uint32_t bufferSize, uint8_t address);
    esp_err_t sendData(uint8_t data[], uint32_t dataSize, uint8_t address);
    esp_err_t sendLarge(uint8_t data[], int dataSize, uint8_t address);



    void reconnect(uint8_t address);
    void setDeviceAddresses(uint8_t addresses[]);

    uint8_t getSocketNum(uint8_t address);

private:
    static void wifi_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data);
    void do_retransmit(const int sock);

    esp_err_t getClientDeviceAddress(uint8_t socketNum);
    esp_err_t receiveDeviceAddress(uint8_t socketNum, uint8_t &address);
    void createListenSocket();

    void closeSocket(uint8_t socketNum);
    void createSocket(uint8_t socketNum);

    void appendBuffer(uint8_t *buffer, uint32_t &startIndex, uint8_t* newData, int newDataSize);

};

#endif
