#include "config.h"
// #include "NetworkClient.h"
#include "Manager.h"

#include <mutex>

static const char* TAG = "main";

/*
TODO:
- change buffer to vector
*/

// NetworkClient networkClient;
Manager manager;

static esp_err_t i2c_init()
{
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_SDA_PIN;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_SCL_PIN;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_FREQ_HZ;
    conf.clk_flags = 0;

    i2c_param_config(I2C_MASTER_NUM, &conf);

    // TODO handle error
    esp_err_t ret = i2c_driver_install(I2C_MASTER_NUM, conf.mode,
                                 I2C_RX_BUF_DISABLE, I2C_TX_BUF_DISABLE, 0);

    return ret;
}

extern "C" void app_main(void)
{
    esp_err_t ret;
    ret = i2c_init();
    CHECK(TAG, ret, "I2C init");

    // networkClient.init();
    // CHECK(TAG, ret, "networkClient init");

    manager.init();
    CHECK(TAG, ret, "manager init");

    manager.run();

    // while(1)
    // {
    //     // sht30.updateMeasuredData();
    //     // bmp180.updateMeasuredData();
    //     // tsl2591.updateMeasuredData();
    //     // distanceSensors.updateAllSensorsData();

    //     // printf("\n");

    //     // motorDriver.vehicleForward(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();
    //     // vTaskDelay(1000 / portTICK_RATE_MS);        
    //     // motorDriver.vehicleBackward(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();
    //     // vTaskDelay(1000 / portTICK_RATE_MS);        

    //     // motorDriver.vehicleRight(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();
    //     // vTaskDelay(1000 / portTICK_RATE_MS);        
    //     // motorDriver.vehicleLeft(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();   
    //     // vTaskDelay(1000 / portTICK_RATE_MS);         

    //     // motorDriver.vehicleForwardRight(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();
    //     // vTaskDelay(1000 / portTICK_RATE_MS);        
    //     // motorDriver.vehicleBackwardLeft(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();   
    //     // vTaskDelay(1000 / portTICK_RATE_MS);

    //     // motorDriver.vehicleForwardLeft(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();
    //     // vTaskDelay(1000 / portTICK_RATE_MS);        
    //     // motorDriver.vehicleBackwardRight(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();           
    //     // vTaskDelay(1000 / portTICK_RATE_MS);

    //     // motorDriver.vehicleTurnAroundRight(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();
    //     // vTaskDelay(1000 / portTICK_RATE_MS);        
    //     // motorDriver.vehicleTurnAroundLeft(80.0f);
    //     // vTaskDelay(1000 / portTICK_RATE_MS);
    //     // motorDriver.vehicleStop();           
    //     // vTaskDelay(1000 / portTICK_RATE_MS);        
     
    //     // printf("Temperature: %f°C\n", sht30.getTemperature());
    //     // printf("Humidity: %f%%\n", sht30.getHumidity());

    //     // printf("Air pressure: %iPa\n", bmp180.getAirPressure());
    //     // //printf("Absolute height: %fm\n", bmp180.getAbsoluteHeight());
    //     // printf("BMP180 temperature: %f°C\n", bmp180.getTemperature());

    //     // printf("Light intensity CH0: %i\n", tsl2591.getLightIntensityRaw(0));
    //     // printf("Light intensity CH1: %i\n", tsl2591.getLightIntensityRaw(1));
    //     // printf("Lux: %f\n", tsl2591.getLux());

    //     // printf("Ultrasonic distance FRONT: %i\n", distanceSensors.getDistanceCm(FRONT));
    //     // printf("Ultrasonic distance RIGHT: %i\n", distanceSensors.getDistanceCm(RIGHT));
    //     // printf("Ultrasonic distance FRONT: %i\n", distanceSensors.getDistanceCm(BACK));
    //     // printf("Ultrasonic distance RIGHT: %i\n", distanceSensors.getDistanceCm(LEFT));        

    //     // printf("\n");



    //     // vTaskDelay(2000 / portTICK_RATE_MS);
    // }
}