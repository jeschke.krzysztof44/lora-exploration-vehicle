/*
    NetworkServer.cpp
    
    Author:     Krzysztof Jeschke
    Created:    31.05.2022
*/

#include "NetworkServer.h"

NetworkServer::NetworkServer()
{

}

NetworkServer::~NetworkServer()
{
}



esp_err_t NetworkServer::init()
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    return ret;
}

void NetworkServer::wifi_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    if (event_id == WIFI_EVENT_AP_STACONNECTED) {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" join, AID=%d",
                 MAC2STR(event->mac), event->aid);
    } else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }
}

void NetworkServer::wifi_init_softap()
{
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_ap();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));


    wifi_config_t wifi_config = { .ap =  {
        EXAMPLE_ESP_WIFI_SSID,// ssid[32]
        EXAMPLE_ESP_WIFI_PASS,// password[64]
        strlen(EXAMPLE_ESP_WIFI_SSID),// ssid_len
        EXAMPLE_ESP_WIFI_CHANNEL,// channel
        WIFI_AUTH_WPA_WPA2_PSK,// authmode
        false, // ssid_hidden;
        EXAMPLE_MAX_STA_CONN,// max_connection
        {},// beacon_interval
        {},// pairwise_cipher
        {}// ftm_responder
    }};    

    

    if (strlen(EXAMPLE_ESP_WIFI_PASS) == 0) {
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s channel:%d",
             EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS, EXAMPLE_ESP_WIFI_CHANNEL);
}



void NetworkServer::setDeviceAddresses(uint8_t addresses[])
{
    uint8_t arraySize = sizeof(addresses) / sizeof(addresses[0]);

    for(uint8_t i = 0; i < arraySize; i ++)
    {
        this->deviceAddrAndSocket.insert( std::pair<uint8_t, uint8_t>(addresses[i], 0) );
    }
}

uint8_t NetworkServer::getSocketNum(uint8_t address)
{
    return this->deviceAddrAndSocket.at(address);
}

esp_err_t NetworkServer::sendData(uint8_t data[], uint32_t dataSize, uint8_t address)
{
    uint8_t socketNum = this->deviceAddrAndSocket.at(address);

    uint8_t to_write = dataSize;
    while (to_write > 0) 
    {
        int written = send(this->sockets[socketNum], data + (dataSize - to_write), to_write, 0);
        if (written < 0) 
        {
            ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
            return ESP_FAIL;
        }
        to_write -= written;
    }
    ESP_LOGI(TAG, "Sent %d bytes", dataSize);
    return ESP_OK;
}

void NetworkServer::waitForClients()
{
    esp_err_t ret;
    uint8_t failMaxNum = 5;
    uint8_t failNum = 0;
    createListenSocket();

    for(uint8_t i = 0; i < CLIENTS_NUM; i++)
    {
        createSocket(i);
        do
        {
            ret = getClientDeviceAddress(i);
        }
        while(ret != ESP_OK && ++failNum < failMaxNum);
        if(ret != ESP_OK)
        {
            ESP_LOGE(TAG, "Cannot get proper device address for socket number: %i", i);
        }
    }
}

void NetworkServer::createListenSocket()
{
    int addr_family = AF_INET;
    int ip_protocol = 0;
    struct sockaddr_storage dest_addr;

    struct sockaddr_in *dest_addr_ip4 = (struct sockaddr_in *)&dest_addr;
    //dest_addr_ip4->sin_addr.s_addr = htonl(INADDR_ANY);
    //dest_addr_ip4->sin_addr.s_addr = inet_addr(IP_ADDRESS);
    dest_addr_ip4->sin_family = AF_INET;
    dest_addr_ip4->sin_port = htons(PORT);

    ip_protocol = IPPROTO_IP;

    bool success = false;

    while (!success) 
    {

        this->listenSocket = socket(addr_family, SOCK_STREAM, ip_protocol);

        if (this->listenSocket < 0) 
        {
            ESP_LOGE(TAG, "Unable to create socket: errno %d", errno);
            continue;
        }
        int opt = 1;
        setsockopt(this->listenSocket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

        ESP_LOGI(TAG, "Socket created");

        int err = bind(this->listenSocket, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (err != 0) 
        {
            ESP_LOGE(TAG, "Socket unable to bind: errno %d", errno);
            ESP_LOGE(TAG, "IPPROTO: %d", addr_family);
            close(this->listenSocket);
            continue;
        }
        ESP_LOGI(TAG, "Socket bound, port %d", PORT);

        err = listen(this->listenSocket, 1);
        if (err != 0) 
        {
            ESP_LOGE(TAG, "Error occurred during listen: errno %d", errno);
            close(this->listenSocket);
            continue;
        }

        ESP_LOGI(TAG, "Socket listening");
        success = true;
    }
}

void NetworkServer::createSocket(uint8_t socketNum)
{
 
    char addr_str[128];
    int keepAlive = 1;
    int keepIdle = KEEPALIVE_IDLE;
    int keepInterval = KEEPALIVE_INTERVAL;
    int keepCount = KEEPALIVE_COUNT;

    bool success = false;

    while (!success) 
    {
        ESP_LOGI(TAG, "Waiting for client num: %i", socketNum);
        struct sockaddr_storage source_addr; // Large enough for both IPv4 or IPv6
        socklen_t addr_len = sizeof(source_addr);
        this->sockets[socketNum] = accept(this->listenSocket, (struct sockaddr *)&source_addr, &addr_len);
        if (this->sockets[socketNum] < 0) 
        {
            ESP_LOGE(TAG, "Unable to accept connection: errno %d", errno);
            continue;
        }

        // Set tcp keepalive option
        setsockopt(this->sockets[socketNum], SOL_SOCKET, SO_KEEPALIVE, &keepAlive, sizeof(int));
        setsockopt(this->sockets[socketNum], IPPROTO_TCP, TCP_KEEPIDLE, &keepIdle, sizeof(int));
        setsockopt(this->sockets[socketNum], IPPROTO_TCP, TCP_KEEPINTVL, &keepInterval, sizeof(int));
        setsockopt(this->sockets[socketNum], IPPROTO_TCP, TCP_KEEPCNT, &keepCount, sizeof(int));

        // Convert ip address to string
        if (source_addr.ss_family == PF_INET) 
        {
            inet_ntoa_r(((struct sockaddr_in *)&source_addr)->sin_addr, addr_str, sizeof(addr_str) - 1);
        }

        ESP_LOGI(TAG, "Socket accepted ip address: %s", addr_str);
        
        success = true;
    }

}

esp_err_t NetworkServer::getClientDeviceAddress(uint8_t socketNum)
{
    uint8_t deviceAddr;
    esp_err_t ret = receiveDeviceAddress(socketNum, deviceAddr);
    // esp_err_t ret = receiveData(socketNum);

    if(ret != ESP_OK)
    {
        return ret;
    }
    
    if(this->deviceAddrAndSocket.find(deviceAddr) == this->deviceAddrAndSocket.end())
    {
        ESP_LOGE(TAG, "No such device address: %i", deviceAddr);
        return ESP_FAIL;
    }

    this->deviceAddrAndSocket.at(deviceAddr) = socketNum;

    ESP_LOGI(TAG, "Device address %i added.", deviceAddr);

    return ESP_OK;
}

esp_err_t NetworkServer::receiveDeviceAddress(uint8_t socketNum, uint8_t &address)
{
    uint8_t addressBuffer[TCP_BUFFER_SIZE];
    uint8_t receivedDataNum;

    receivedDataNum = recv(this->sockets[socketNum], addressBuffer, sizeof(addressBuffer) - 1, 0);
    if (receivedDataNum < 0) 
    {
        ESP_LOGE(TAG, "Error occurred during receiving device address: errno %d", errno);
        return ESP_FAIL;
    } 
    else if (receivedDataNum == 0) 
    {
        ESP_LOGW(TAG, "Connection closed");
        return ESP_FAIL;
    }
    
    addressBuffer[receivedDataNum] = 0; // Null-terminate whatever is received and treat it like a string
    ESP_LOGI(TAG, "Received address: %s", addressBuffer);

    address = addressBuffer[0];

    return ESP_OK;
}

void NetworkServer::closeSocket(uint8_t socketNum)
{
    shutdown(this->sockets[socketNum], 0);
    close(this->sockets[socketNum]);
}

void NetworkServer::reconnect(uint8_t address)
{
    uint8_t socketNum = this->deviceAddrAndSocket.at(address);
    closeSocket(socketNum);
    createSocket(socketNum);
}

int32_t NetworkServer::receiveData(uint8_t buffer[], uint32_t bufferSize, uint8_t address)
{
    uint8_t socketNum = this->deviceAddrAndSocket.at(address);
    

    int32_t receivedFrameSize = recv(this->sockets[socketNum], buffer, bufferSize, 0);
    if (receivedFrameSize < 0) 
    {
        ESP_LOGE(TAG, "Error occurred during receiving: errno %d", errno);
        return receivedFrameSize;
    } 
    else if (receivedFrameSize == 0) 
    {
        ESP_LOGW(TAG, "Connection closed");
        return receivedFrameSize;
    }
    
    //this->receiveBuffer[this->receivedFrameSize] = 0; // Null-terminate whatever is received and treat it like a string
    ESP_LOGI(TAG, "Received %d bytes", receivedFrameSize);
    return receivedFrameSize;

    // send() can return less bytes than supplied length.
    // Walk-around for robust implementation.
    // int to_write = len;
    // while (to_write > 0) {
    //     int written = send(sock, rx_buffer + (len - to_write), to_write, 0);
    //     if (written < 0) {
    //         ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
    //     }
    //     to_write -= written;
    // }
     
}