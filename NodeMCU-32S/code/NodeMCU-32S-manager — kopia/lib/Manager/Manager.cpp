/*
    Manager.cpp
    
    Author:     Krzysztof Jeschke
    Created:    31.05.2022
*/

#include "Manager.h"

Manager::Manager()
{
}

Manager::~Manager()
{
}

esp_err_t Manager::init()
{
    esp_err_t ret = sht30.init();
    CHECK(TAG, ret, "sht30 init");

    ret = bmp180.init();
    CHECK(TAG, ret, "bmp180 init");

    ret = tsl2591.init();
    CHECK(TAG, ret, "tsl2591 init");

    ret = distanceSensors.init();
    CHECK(TAG, ret, "distanceSensors init");
    
    motorDriver.init();
    CHECK(TAG, ret, "motorDriver init");    

    this->networkServer.init();
    CHECK(TAG, ret, "networkServer init");    

    this->networkServer.wifi_init_softap();

    this->networkServer.setDeviceAddresses(this->deviceAddresses);
    this->networkServer.waitForClients();

    return ESP_OK;
}

void Manager::run()
{
    printf("Waiting for commands \n");
    while(1)
    {
        receiveData(MY_ADDRESS);
        if (isNewData(MY_ADDRESS))
        {
            runReceivedCommands();
        }
    }
}

void Manager::receiveData(Commands address)
{   
    if (this->networkServer.receiveData(address) != ESP_OK) this->networkServer.reconnect(address);
}

void Manager::sendData(uint8_t data[], uint8_t dataSize, Commands address)
{
    if (this->networkServer.sendData(data, dataSize, address) != ESP_OK) this->networkServer.reconnect(address);
}

bool Manager::isNewData(Commands address)
{
    uint8_t socketNum = this->networkServer.getSocketNum(address);
    return this->networkServer.receivedFrameSize != 0 ? true : false;
}

void Manager::runReceivedCommands()
{
    uint8_t responseBuffer[TCP_BUFFER_SIZE];
    uint32_t currentBufferIndex = 0;
    responseBuffer[currentBufferIndex++] = Manager::MY_ADDRESS;

    uint8_t mySocketNum = this->networkServer.getSocketNum(Manager::MY_ADDRESS);
    uint8_t espCamSocketNum = this->networkServer.getSocketNum(Manager::ESP_CAM_ADDR);

    uint8_t espCamSendCommand[1];

    for(uint8_t i = 0; i < this->networkServer.receivedFrameSize[mySocketNum]; i++)
    {
        // printf("%i\n", (int)this->networkServer.receiveBuffer[i]);
        int move_seconds;

        switch (this->networkServer.receiveBuffer[mySocketNum][i])
        {
        case Manager::MOVE_FORWARD:
            move_seconds = (int)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_FORWARD for %i seconds\n", move_seconds);
            motorDriver.vehicleForward(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();
            break;

        case Manager::MOVE_BACKWARD:
            move_seconds = (uint8_t)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_BACKWARD for %i seconds\n", move_seconds);
            motorDriver.vehicleBackward(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();            
            break;            

        case Manager::MOVE_RIGHT:
            move_seconds = (uint8_t)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_RIGHT for %i seconds\n", move_seconds);
            motorDriver.vehicleRight(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();            
            break;

        case Manager::MOVE_LEFT:
            move_seconds = (uint8_t)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_LEFT for %i seconds\n", move_seconds);
            motorDriver.vehicleLeft(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();            
            break;   

        case Manager::MOVE_FORWARD_RIGHT:
            move_seconds = (uint8_t)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_FORWARD for %i seconds\n", move_seconds);
            motorDriver.vehicleForwardRight(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();            
            break;

        case Manager::MOVE_FORWARD_LEFT:
            move_seconds = (uint8_t)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_BACKWARD for %i seconds\n", move_seconds);
            motorDriver.vehicleForwardLeft(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();            
            break;            

        case Manager::MOVE_BACKWARD_RIGHT:
            move_seconds = (uint8_t)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_RIGHT for %i seconds\n", move_seconds);
            motorDriver.vehicleBackwardRight(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();            
            break;

        case Manager::MOVE_BACKWARD_LEFT:
            move_seconds = (uint8_t)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_LEFT for %i seconds\n", move_seconds);
            motorDriver.vehicleBackwardLeft(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();            
            break;     

        case Manager::MOVE_TURN_AROUND_LEFT:
            move_seconds = (uint8_t)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_RIGHT for %i seconds\n", move_seconds);
            motorDriver.vehicleTurnAroundLeft(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();            
            break;

        case Manager::MOVE_TURN_AROUND_RIGHT:
            move_seconds = (uint8_t)this->networkServer.receiveBuffer[mySocketNum][++i];
            printf("MOVE_LEFT for %i seconds\n", move_seconds);
            motorDriver.vehicleTurnAroundRight(60.0f);
            sleep_s(move_seconds);
            motorDriver.vehicleStop();            
            break;    

        case Manager::GET_TEMPERATURE:
            this->sht30.updateMeasuredData();
            this->float_uint8_t_arr.floatData = this->sht30.getTemperature();

            appendBuffer(responseBuffer, currentBufferIndex, float_uint8_t_arr.uint8_t_Data, sizeof(float));
            //strncat(responseBuffer, float_uint8_t_arr.charData, sizeof(float));
            break;

        case Manager::GET_HUMIDITY:
            break;

        case Manager::GET_LIGHT_INTENSITY:
            this->tsl2591.updateMeasuredData();
            this->float_uint8_t_arr.floatData = this->tsl2591.getLux();

            appendBuffer(responseBuffer, currentBufferIndex, float_uint8_t_arr.uint8_t_Data, sizeof(float));
            break;

        case Manager::GET_ATM_PRESSURE:
            this->bmp180.updateMeasuredData();
            // this->uint32Char.uint32Data = this->bmp180.getAirPressure();
            // printf("Air pressure: %iPa\n", bmp180.getAirPressure());
            this->float_uint8_t_arr.floatData = ((float)bmp180.getAirPressure()) / 100;
            printf("Air pressure: %fhPa\n", ((float)bmp180.getAirPressure()) / 100);            

            appendBuffer(responseBuffer, currentBufferIndex, float_uint8_t_arr.uint8_t_Data, sizeof(float));
            break; 

        case Manager::TAKE_PHOTO:
            espCamSendCommand[0] = Manager::TAKE_PHOTO;
            sendData(espCamSendCommand, 1, Manager::ESP_CAM_ADDR);
            receiveData(Manager::ESP_CAM_ADDR);
            appendBuffer(responseBuffer, currentBufferIndex, this->networkServer.receiveBuffer[espCamSocketNum], this->networkServer.receivedFrameSize[espCamSocketNum]);
            break;                            

        case Manager::MOVE_GIMBAL_UP:
            espCamSendCommand[0] = Manager::MOVE_GIMBAL_UP;
            espCamSendCommand[1] = this->networkServer.receiveBuffer[mySocketNum][++i];

            sendData(espCamSendCommand, 2, Manager::ESP_CAM_ADDR);
            receiveData(Manager::ESP_CAM_ADDR);
            appendBuffer(responseBuffer, currentBufferIndex, this->networkServer.receiveBuffer[espCamSocketNum], 1);
            break;

        case Manager::MOVE_GIMBAL_DOWN:
            espCamSendCommand[0] = Manager::MOVE_GIMBAL_DOWN;
            espCamSendCommand[1] = this->networkServer.receiveBuffer[mySocketNum][++i];

            sendData(espCamSendCommand, 2, Manager::ESP_CAM_ADDR);
            receiveData(Manager::ESP_CAM_ADDR);
            appendBuffer(responseBuffer, currentBufferIndex, this->networkServer.receiveBuffer[espCamSocketNum], 1);   
            break;

        case Manager::MOVE_GIMBAL_LEFT:
            espCamSendCommand[0] = Manager::MOVE_GIMBAL_LEFT;
            espCamSendCommand[1] = this->networkServer.receiveBuffer[mySocketNum][++i];

            sendData(espCamSendCommand, 2, Manager::ESP_CAM_ADDR);
            receiveData(Manager::ESP_CAM_ADDR);
            appendBuffer(responseBuffer, currentBufferIndex, this->networkServer.receiveBuffer[espCamSocketNum], 1);   
            break;

        case Manager::MOVE_GIMBAL_RIGHT:
            espCamSendCommand[0] = Manager::MOVE_GIMBAL_RIGHT;
            espCamSendCommand[1] = this->networkServer.receiveBuffer[mySocketNum][++i];

            sendData(espCamSendCommand, 2, Manager::ESP_CAM_ADDR);
            receiveData(Manager::ESP_CAM_ADDR);
            appendBuffer(responseBuffer, currentBufferIndex, this->networkServer.receiveBuffer[espCamSocketNum], 1);    
            break;                        
        
        default:
            break;
        }
    }   

    sendData(responseBuffer, currentBufferIndex, MY_ADDRESS);

    this->networkServer.receivedFrameSize[mySocketNum] = 0;         
}

void Manager::appendBuffer(uint8_t *buffer, uint32_t &startIndex, uint8_t* newData, uint32_t newDataSize)
{
    // buffer[startIndex++] = newData[0];
    // buffer[startIndex++] = newData[1];
    // buffer[startIndex++] = newData[2];
    // buffer[startIndex++] = newData[3];


    for(int i = 0; i < newDataSize; i++)
    {
        buffer[startIndex++] = newData[i];        
    }    
}
