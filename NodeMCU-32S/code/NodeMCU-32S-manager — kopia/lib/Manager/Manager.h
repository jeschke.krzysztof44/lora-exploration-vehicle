/*
    Manager.h
    
    Author:     Krzysztof Jeschke
    Created:    31.05.2022
*/

#pragma once

#ifndef __Manager_H__
#define __Manager_H__

#include "SHT30.h"
#include "BMP180.h"
#include "TSL2591.h"
#include "UltrasonicSensorsManager.h"
#include "MotorDriver.h"
#include "NetworkServer.h"
#include "../../include/config.h"

#include <string.h>


class Manager
{

// variables
public:
    union
    {
        float floatData;
        uint8_t uint8_t_Data[sizeof(float)];
    } float_uint8_t_arr;

    union
    {
        uint32_t uint32Data;
        char uint8_t_Data[sizeof(float)];
    } uint32_t_uint8_t_arr;    
    
private:
    SHT30 sht30; // temperature & humidity
    BMP180 bmp180; // atmospheric pressure
    TSL2591 tsl2591; // light intensity
    UltrasonicSensorsManager distanceSensors;
    MotorDriver motorDriver;
    NetworkServer networkServer;
    

    static constexpr const char *TAG = "Manager";

    enum Commands
    {
        MY_ADDRESS = (uint8_t)0,

        MOVE_FORWARD = (uint8_t)10,
        MOVE_BACKWARD = (uint8_t)11,
        MOVE_RIGHT = (uint8_t)12,
        MOVE_LEFT = (uint8_t)13,
        MOVE_FORWARD_RIGHT = (uint8_t)14,
        MOVE_FORWARD_LEFT = (uint8_t)15,
        MOVE_BACKWARD_RIGHT = (uint8_t)16,
        MOVE_BACKWARD_LEFT = (uint8_t)17,
        MOVE_TURN_AROUND_LEFT = (uint8_t)18,
        MOVE_TURN_AROUND_RIGHT = (uint8_t)19,

        GET_TEMPERATURE = (uint8_t)20,
        GET_HUMIDITY = (uint8_t)21,
        GET_LIGHT_INTENSITY = (uint8_t)22,
        GET_ATM_PRESSURE = (uint8_t)23,
        GET_DISTANCE_FRONT = (uint8_t)24,
        GET_DISTANCE_RIHGT = (uint8_t)25,
        GET_DISTANCE_BACK = (uint8_t)26,
        GET_DISTANCE_LEFT =(uint8_t)27,
        GET_ALL = (uint8_t)28, // get all measured values

        ESP_CAM_ADDR = (uint8_t)1,

        TAKE_PHOTO = (uint8_t)40,
        MOVE_GIMBAL_UP = (uint8_t)41,
        MOVE_GIMBAL_DOWN = (uint8_t)42,
        MOVE_GIMBAL_RIGHT = (uint8_t)43,
        MOVE_GIMBAL_LEFT = (uint8_t)44,

        ERROR = 99,
    };      

    uint8_t deviceAddresses[CLIENTS_NUM] = {MY_ADDRESS, ESP_CAM_ADDR};
// methods
public:
    Manager();
    ~Manager();

    esp_err_t init();
    void run();

private:
    bool isNewData(Commands address);
    void runReceivedCommands();
    void sendData(uint8_t data[], uint8_t dataSize, Commands address);
    void receiveData(Commands address);

    void appendBuffer(uint8_t *buffer, uint32_t &startIndex, uint8_t* newData, uint32_t newDataSize);

    float ieee_float(uint32_t f);
};

#endif