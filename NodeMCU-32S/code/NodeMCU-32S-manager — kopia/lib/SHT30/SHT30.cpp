/*
    SHT30.cpp
    
    Author:     Krzysztof Jeschke
    Created:    10.04.2022
*/

#include "SHT30.h"

SHT30::SHT30()
{
}

SHT30::~SHT30()
{
}

esp_err_t SHT30::init()
{
    return ESP_OK;
}

esp_err_t SHT30::updateMeasuredData()
{
    esp_err_t ret = startMeasurement();
    if (ret != ESP_OK)
    {
        ESP_LOGE(TAG, "Starting measurtement failed.");
        return ret;
    }

    ret = readMeasuredData();
    if (ret != ESP_OK)
    {
        ESP_LOGE(TAG, "Reading measurtement failed.");
        return ret;
    }

    return ESP_OK;
}

esp_err_t SHT30::startMeasurement()
{
    uint8_t upperCommandByte = (uint8_t) (MEASUREMENT_COMMAND >> 8);
    uint8_t lowerCommandByte = (uint8_t) MEASUREMENT_COMMAND;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);

    i2c_master_write_byte(cmd, (I2C_SHT30_ADDRESS << 1) | WRITE_BIT, ACK_CHECK_EN);

    i2c_master_write_byte(cmd, upperCommandByte, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, lowerCommandByte, ACK_CHECK_EN);

    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);

    return ret;
}



esp_err_t SHT30::readMeasuredData()
{
    uint8_t temperatureMSB = 0;
    uint8_t temperatureLSB = 0;
    uint8_t humidityMSB = 0;
    uint8_t humidityLSB = 0;

    uint8_t temperatureCRC = 0;
    uint8_t humidityCRC = 0;

    for (int i = 0; i < MAX_READ_NUM; i++)
    {
        i2c_cmd_handle_t cmd = i2c_cmd_link_create();
        i2c_master_start(cmd);
        
        i2c_master_write_byte(cmd, (I2C_SHT30_ADDRESS << 1) | READ_BIT, ACK_CHECK_EN);

        //read temperature
        i2c_master_read_byte(cmd, &temperatureMSB, ACK_VAL);
        i2c_master_read_byte(cmd, &temperatureLSB, ACK_VAL);
        //TODO check CRC
        i2c_master_read_byte(cmd, &temperatureCRC, ACK_VAL);

        //read humidity
        i2c_master_read_byte(cmd, &humidityMSB, ACK_VAL);
        i2c_master_read_byte(cmd, &humidityLSB, ACK_VAL);
        //TODO check CRC
        i2c_master_read_byte(cmd, &humidityCRC, NACK_VAL);

        i2c_master_stop(cmd);
        esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);

        i2c_cmd_link_delete(cmd);

        this->temperatureRaw = ((uint16_t) temperatureMSB << 8) | temperatureLSB;
        this->humidityRaw = ((uint16_t) humidityMSB << 8) | humidityLSB;
        
        //convert raw values
        this->temperature = (float) (-45 + 175 * (float)(this->temperatureRaw)/65535);
        this->humidity = (float) (100 * (float)(this->humidityRaw)/65535);


        if (ret == ESP_OK)
        {
            return ESP_OK;
        }
        ESP_LOGE(TAG, "Read data try num %i failed, trying again.", i);

        vTaskDelay(5 / portTICK_RATE_MS);
    }
    return ESP_FAIL;    
}

//TODO change it
uint8_t SHT30::crc8(uint8_t MSB, uint8_t LSB)
{
    uint8_t crc = 0xff;

    crc ^= MSB;
    for (int i = 0; i < 8; i++)
    {
        bool isXxor = crc & 0x80;
        crc = crc << 1;
        crc = isXxor ? crc ^ POLYNOMIAL : crc;
    }

    crc ^= LSB;
    for (int i = 0; i < 8; i++)
    {
        bool isXxor = crc & 0x80;
        crc = crc << 1;
        crc = isXxor ? crc ^ POLYNOMIAL : crc;
    }

    return crc;
}

float SHT30::getTemperature()
{
    return this->temperature;
}

float SHT30::getHumidity()
{
    return this->humidity;
}