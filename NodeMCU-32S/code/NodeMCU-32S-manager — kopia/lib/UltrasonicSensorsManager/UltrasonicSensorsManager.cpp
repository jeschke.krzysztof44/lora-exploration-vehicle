/*
    UltrasonicSensorsManager.cpp
    
    Author:     Krzysztof Jeschke
    Created:    25.04.2022
*/

#include "UltrasonicSensorsManager.h"

UltrasonicSensorsManager::UltrasonicSensorsManager()
{

}

UltrasonicSensorsManager::~UltrasonicSensorsManager()
{
}

esp_err_t UltrasonicSensorsManager::init()
{    
    esp_err_t errorCode = ESP_OK;

    for(auto &devicePair : this->ultrasonicDevices)
    {
        if (devicePair.second.init() != ESP_OK)
        {
            errorCode = ESP_FAIL;
        }
    }

    return errorCode;
}

esp_err_t UltrasonicSensorsManager::updateAllSensorsData()
{
    // fixes problem with serial pins
    vTaskDelay(100 / portTICK_RATE_MS);
    esp_err_t errorCode = ESP_OK;
    for(auto &devicePair : this->ultrasonicDevices)
    {
        if (devicePair.second.updateMeasuredData() != ESP_OK)
        {
            errorCode = ESP_FAIL;
        }
    }

    return errorCode;
}

uint32_t UltrasonicSensorsManager::getDistanceCm(UltrasonicDevPosition devPosition)
{

    return this->ultrasonicDevices.at(devPosition).getDistanceCm();
}