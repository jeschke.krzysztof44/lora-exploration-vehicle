/*
    NetworkClient.h
    
    Author:     Krzysztof Jeschke
    Created:    25.04.2022
*/

#pragma once

#ifndef __NetworkClient_H__
#define __NetworkClient_H__

#include <string.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "lwip/err.h"
#include "lwip/sockets.h"

#include <mutex>

#include "../../include/config.h"
#include "../../include/node_mcu_private.h"

#define ESP_MAXIMUM_RETRY  5

#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_WPA2_PSK

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

#define HOST_IP_ADDR "192.168.4.1"
#define PORT 10001

volatile static int s_retry_num;
volatile static EventGroupHandle_t s_wifi_event_group;

class NetworkClient
{

// variables
public:

    // struct TcpData
    // {
    //     std::mutex tcp_buffer_mutex;
    //     char receiveBuffer[TCP_BUFFER_SIZE];
    //     uint8_t receivedFrameSize = 0;

    //     char send_buffer[TCP_BUFFER_SIZE];
    //     uint8_t sent_frame_size = 0;
    // };
    char receiveBuffer[TCP_BUFFER_SIZE];
    uint8_t receivedFrameSize = 0;

private:
    static constexpr const char *TAG = "NetworkClient";

    int currentSocket;

    // char send_buffer[TCP_BUFFER_SIZE];
    // uint8_t sent_frame_size = 0;

    bool enableSending = false;
    bool enableReceiving = false;

    // TcpData *tcpData;

// methods
public:
    NetworkClient();
    ~NetworkClient();

    // esp_err_t init(TcpData &tcpData);
    esp_err_t init();
    void wifi_init_sta(void);
    void tcp_client_task();

    esp_err_t sendData(char data[], uint8_t dataSize);
    esp_err_t receiveData();
    void createSocket();
    void reconnect();

private:
    static void event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data);
    void closeSocket();

    esp_err_t internalReceive();
    esp_err_t internalSend();    
};

#endif