/*
    BMP180.cpp
    
    Author:     Krzysztof Jeschke
    Created:    10.04.2022
*/

#include "BMP180.h"

BMP180::BMP180()
{
}

BMP180::~BMP180()
{
}

esp_err_t BMP180::init()
{
    return bmp180_init(I2C_SDA_PIN, I2C_SCL_PIN);
}

esp_err_t BMP180::updateMeasuredData()
{

    esp_err_t ret = bmp180_read_pressure(&this->airPressure);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Reading of pressure failed, err = %d", ret);
    }
    ret = bmp180_read_altitude(REFERENCE_PRESSURE, &this->absoluteHeight);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Reading of altitude failed, err = %d", ret);
    }
    ret = bmp180_read_temperature(&this->temperature);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Reading of temperature failed, err = %d", ret);
    }

    return ret;
}

uint32_t BMP180::getAirPressure()
{
    return this->airPressure;
}

float BMP180::getAbsoluteHeight()
{
    return this->absoluteHeight;
}

float BMP180::getTemperature()
{
    return this->temperature;
}