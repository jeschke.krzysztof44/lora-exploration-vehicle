/*
    BMP180.h
    
    Author:     Krzysztof Jeschke
    Created:    10.04.2022
*/

#pragma once

#ifndef __BMP180_H__
#define __BMP180_H__

#include "../../include/config.h"
#include "bmp180-driver.h"
#include "math.h"

#define READ_AIR_PRESSURE_REG_ADDR  (uint8_t) 0xf6
#define MAX_READ_NUM                (uint8_t) 10
#define REFERENCE_PRESSURE          101325l 

class BMP180
{

// variables
public:

private:
    static constexpr const char *TAG = "BMP180";
    
    uint32_t airPressure;
    float absoluteHeight;
    float temperature;

// methods
public:
    BMP180();
    ~BMP180();

    esp_err_t init();

    uint32_t getAirPressure();
    float getAbsoluteHeight();
    float getTemperature();
    esp_err_t updateMeasuredData();

private:
};

#endif