import socket
import errno
from time import sleep


"""
vehicle movements 'mx;d':
m - move
x - direction
d - duration in seconds

get measured value gx:
g - get
x - measured value

take photo 'tf'

move gimbal 'mgx;a'
mg - move gimbal
x - direction
a - angle (absolute)


e.g. send frame:    man_addr mf 5 ga cam_addr tf
                    vehicle moves foraward, duration 5s, measures all values, takes a photo

data is returned in the same orderd which was requested. When selected "ga" (all data), data is returned in order:
1) temperature
2) humidity
3) light intensity
4) atmospheric pressure
5) distance front
6) distance left
7) distance back
8) distance right
"""
COMMANDS =  {
                "node_addr" : 0, # manager device address

                "mf" : 10, # forward
                "mb" : 11, # backward
                "mr" : 12, # right
                "ml" : 13, # left
                "mfr" : 14, # forward-right
                "mfl" : 15, # forward-left
                "mbr" : 16, # backward=right
                "mbl" : 17, # backward-left
                "mtr" : 18, # turn around right
                "mtl" : 19, # turn around left

                "gt" : 20, # temperature
                "gh" : 21, # humidity
                "gl" : 22, # light intensity
                "gp" : 23, # atmospheric pressure
                "gdf" : 24, # distance front
                "gdr" : 25, # distance right
                "gdb" : 26, # distance back
                "gdl" : 27, # distance left
                "ga" : 28, # get all measured values

                "cam_addr" : 1, # manager device address
                
                "tf" : 40, # take photo
                "mgu" : 41, # move gimbal up
                "mgd" : 42, # move gimbal down
                "mgr" : 43, # move gimbal right
                "mgl" : 44, # move gimbal left

                "er" : 99, # error
            }

class Client:
    MAX_RECONNECTION_NUMBER = 10

    def __init__(self):
        self._server_ip_address = "192.168.4.1"
        self._server_port = 10001
        self._buffer_size = 1024

        # init client UDP socket
        self.client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
        self.client_socket.connect((self._server_ip_address, self._server_port))

        self._is_new_message = False
        self._send_enable = False
        self._received_message = ''
        self._send_message = ''

        self._current_values =  {
                COMMANDS["gt"] : 0, # temperature
                COMMANDS["gh"] : 0, # humidity
                COMMANDS["gl"] : 0, # light intensity
                COMMANDS["gp"] : 0, # atmospheric pressure
                COMMANDS["gdf"] : 0, # distance front
                COMMANDS["gdr"] : 0, # distance right
                COMMANDS["gdb"] : 0, # distance back
                COMMANDS["gdl"] : 0, # distance left
            }
        self._requested_values = []

        print(f"Connected")

    def run(self):
        self.client_socket.sendall(int.to_bytes(COMMANDS["node_addr"], 1, byteorder='big'))
        stop_sign = "s"
        while(True):
            command = None
            frame = b''
            while command != stop_sign:
                command = input("Write command: ")

                if command == stop_sign:
                    break
                
                try:
                    if command[0] == 'm':

                        if len(command) > 1 and command[1] == 'g':
                            frame += int.to_bytes(COMMANDS[command], 1, byteorder='big')
                            command_angle = input("Write absolute angle: ")
                            frame += int.to_bytes(int(command_angle), 1, byteorder='big')
                            print(f"Current command: {frame}")     
                            continue                       

                        frame += int.to_bytes(COMMANDS[command], 1, byteorder='big')
                        command_time = input("Write time: ")
                        frame += int.to_bytes(int(command_time), 1, byteorder='big')
                        print(f"Current command: {frame}")
                        continue

                    if command[0] == 'g':
                        frame += int.to_bytes(COMMANDS[command], 1, byteorder='big')
                        self._requested_values.append(command)
                        continue

                    frame += int.to_bytes(COMMANDS[command], 1, byteorder='big')
                    print(f"Current command: {frame}")

                except Exception as e:
                    print(e)
                    command = None
                    frame = b''                    


            self.client_socket.sendall(frame)
            print("WAITING FOR RESPONSE...")

            self._socket_receive()
            self._requested_values = []
    
    def is_new_message(self):
        return self._is_new_message
    
    def get_last_message(self):
        return self._received_message

    def send(self, message):
        self._send_enable = True
        self._send_message = message

    def _socket_receive(self):
        try:

            received_message = self.client_socket.recv(self._buffer_size)
            print("Received address: ", received_message[0])

            for command in self._requested_values:
                print(command)

            print(received_message)
            # self._received_message = received_message[0]
            # address = received_message[1]
            
            # print(f"Message from Client: {list(received_message)}")
            # print(f"Float: {struct.unpack('f', received_message[1:])}")
            # print(f"UINT32: {int.from_bytes(received_message[1:], byteorder='little', signed=False)}")

            # message = "TESTTT"

            # self._connection.sendall(message.encode())

            # self._send_message("TEST", address)
        except socket.error as e:
            print(e)

    
    def _socket_send(self, address):
        try:
            self.client_socket.sendto(str.encode(self._send_message) , address)
            print("Message sent")
            self._send_enable = False
            sleep(2)
        except socket.error as e:
            self._socket_check_error(e, 0)

    def _socket_check_error(self, error, test):
        error_code = error.args[0]
        if error_code == errno.EAGAIN or error_code == errno.EWOULDBLOCK:
            pass
        # elif error_code == errno.ECONNRESET:
        #     self._init_socket()
        #     self._receive_message()
        #     self._reconnection_counter += 1
        #     if self._reconnection_counter > Server.MAX_RECONNECTION_NUMBER:
        #         print("Too many reconneciton tries.")
        #         raise
        else:
            print(error)
            raise

if __name__ == "__main__":
    client = Client()
    client.run()
