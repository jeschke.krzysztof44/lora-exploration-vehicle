
                
"""
                vehicle movements 'mx;d':
                m - move
                x - direction
                d - duration in seconds

                get measured value gx:
                g - get
                x - measured value

                take photo 'tf'
"""

COMMANDS =  {
                "man_addr" : 99, # manager device address

                "mf" : 10, # forward
                "mb" : 11, # backward
                "mr" : 12, # right
                "ml" : 13, # left
                "mfr" : 14, # forward-right
                "mfl" : 15, # forward-left
                "mbr" : 16, # backward=right
                "mbl" : 17, # backward-left
                "mtr" : 18, # turn around right
                "mtr" : 19, # turn around left

                "gt" : 20, # temperature
                "gh" : 21, # humidity
                "gl" : 22, # light intensity
                "gp" : 23, # atmospheric pressure
                "gdf" : 24, # distance front
                "gdr" : 25, # distance right
                "gdb" : 26, # distance back
                "gdl" : 27, # distance left
                "ga" : 28, # get all measured values


                "cam_addr" : 98, # manager device address
                
                "tf" : 40,
            }

print(COMMANDS["man_addr"])            