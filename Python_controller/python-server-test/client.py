import socket
import sys

class Client:
    def __init__(self, port):
        self._server_ip_address = "192.168.4.1"
        self._server_port = port
        self._buffer_size = 1024

        # init client UDP socket
        self.client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
        self.client_socket.connect((self._server_ip_address, self._server_port))

    def run(self):
        while(True):
            command = int(input("Write byte (int): "))
            self.client_socket.sendall(int.to_bytes(command, 1, byteorder='big'))

            command = int(input("Write byte (int): "))
            self.client_socket.sendall(int.to_bytes(command, 1, byteorder='big'))
            recevied_message = self.client_socket.recvfrom(self._buffer_size)

            print(f"Recevied message: {recevied_message[0]}")
    
    def receive_message(self):
        recevied_message = self.client_socket.recvfrom(self._buffer_size)
        print(f"Recevied message: {recevied_message[0]}")
 

if __name__ == "__main__":
    server_port = int(sys.argv[1])
    client = Client(port=server_port)
    client.run()