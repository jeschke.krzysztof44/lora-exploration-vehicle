import socket
from PyQt5.QtCore import QObject, pyqtSignal
from time import sleep
import struct

class Client(QObject):
    finished = pyqtSignal()

    def __init__(self, parent=None):
        super(Client, self).__init__(parent)

        self._buffer_size = 96000
        self.send_byte_message = b''
        self.encoded_message = []
        self.expected_data = []
        self.enable_send = False

        # init client UDP socket
        self.client_socket = None


    def connect(self, ip, port):
        self.client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
        self.client_socket.connect((ip, port))


    def receive(self):
        return self.client_socket.recv(self._buffer_size)

    def run(self):
        while(True):
            if not self.enable_send:
                sleep(0.1)
                continue
            
            if len(self.send_byte_message) <= 0:
                self.finished.emit()
                return
                
            print("SENDING")
            self.client_socket.sendall(self.send_byte_message)
            print(self.send_byte_message)

            data_size = self.client_socket.recv(4)
            data_size = struct.unpack('<I', data_size)[0]
            print(f"DATA SIZE: {data_size}")

            data = bytearray()
            while len(data) < data_size:
                packet = self.client_socket.recv(data_size - len(data))
                if not packet:
                    return None

                data.extend(packet)

            print(f"RECEIVED: {data_size} bytes")
            self.decode_data(data[1:])
            self.enable_send = False
            self.finished.emit()

    def decode_data(self, data):
        current_index = 0
        for value in self.expected_data:
            if value == "photo":
                photo_size = len(data) - (len(self.expected_data) - 1) * 4
                photo = data[current_index:current_index+photo_size]
                self.encoded_message.append((value, photo))
                current_index += photo_size
                continue

            new_value = struct.unpack('f', data[current_index:current_index+4])[0]
            self.encoded_message.append((value, new_value))
            current_index +=4

            
            


    


if __name__ == "__main__":
    client = Client()
