COMMANDS =  {
                "node_addr" : 0, # manager device address

                "Vehicle: move forward" : 10, # forward
                "Vehicle: move backward" : 11, # backward
                "Vehicle: move right" : 12, # right
                "Vehicle: move left" : 13, # left
                "Vehicle: move forward-right" : 14, # forward-right
                "Vehicle: move forward-left" : 15, # forward-left
                "Vehicle: move backward=right" : 16, # backward=right
                "Vehicle: move backward-left" : 17, # backward-left
                "Vehicle: turn around right" : 18, # turn around right
                "Vehicle: turn around left" : 19, # turn around left

                "Sensors: get temperature" : 20, # temperature
                "Sensors: get humidity" : 21, # humidity
                "Sensors: get light intensity" : 22, # light intensity
                "Sensors: get atmospheric pressure" : 23, # atmospheric pressure
                "Sensors: get distance front" : 24, # distance front
                "Sensors: get distance right" : 25, # distance right
                "Sensors: get distance back" : 26, # distance back
                "Sensors: get distance left" : 27, # distance left
                "Sensors: get data" : 28, # get all measured values

                "cam_addr" : 1, # manager device address
                
                "Camera: take photo" : 40, # take photo
                "Camera: move gimbal up" : 41, # move gimbal up
                "Camera: move gimbal down" : 42, # move gimbal down
                "Camera: move gimbal right" : 43, # move gimbal right
                "Camera: move gimbal left" : 44, # move gimbal left

                "er" : 99, # error
            }
def tupleToString(in_tuple):
    new_string = ""
    
    return ''.join(in_tuple)

test_tu = [("COMMAND", "22"), ("COMMAND_2", 21)]

string_tu = tupleToString(test_tu[0])

print(string_tu)