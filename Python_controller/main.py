# pyuic5 -x UI/controller.ui -o controller_ui.py

from controller_ui import Ui_MainWindow
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QThread
from controller import VehicleController
import io
import PIL.Image as Image

class Ui_VehicleController(Ui_MainWindow):
    def __init__(self, MainWindow):
        super().__init__()
        self.setupUi(MainWindow)

        self.vehicle_controller = VehicleController()

        # PyQT handlers
        self.add_command_button.clicked.connect(self.add_command)
        self.delete_command_button.clicked.connect(self.delete_command)
        self.connect_button.clicked.connect(self.tcp_connect)
        self.send_button.clicked.connect(self.send_actions)

        self.vehicle_image_label.setPixmap(QtGui.QPixmap("merged_r.png"))
        # self.camera_image_label.setPixmap(QtGui.QPixmap("test.jpg"))
        self.send_button.setEnabled(False)


        # PyQt data
        self.expected_data = []

        self.current_values =   { 
                                    "temperature" : 0,
                                    "humidity" : 0,
                                    "light intensity" : 0,
                                    "atmospheric pressure" : 0,
                                    "distance front" : 0,
                                    "distance right" : 0,
                                    "distance back" : 0,
                                    "distance left" : 0,
                                }

        self.setUI()
        self.createThread()


    def setUI(self):
        self.commands_combo_box.addItems(list(VehicleController.MOVE_COMMANDS))
        self.commands_combo_box.addItems(list(VehicleController.DATA_COMMANDS))

    def createThread(self):
        self.thread = QThread()
        self.vehicle_controller.tcp_client.moveToThread(self.thread)

        self.thread.started.connect(self.vehicle_controller.tcp_client.run)

        self.thread.start()

        self.vehicle_controller.tcp_client.finished.connect(self.update_values)

    def get_photo(self):
        send_byte = b''
        send_byte += self.toByte(VehicleController.DATA_COMMANDS["Sensors: measure all data"])
        send_byte += self.toByte(VehicleController.DATA_COMMANDS["Camera: take photo"])

        for key in self.current_values.keys():
            self.expected_data.append(key)

        self.expected_data.append("photo")

        self.vehicle_controller.tcp_client.encoded_message = []
        self.vehicle_controller.tcp_client.send_byte_message = send_byte
        self.vehicle_controller.tcp_client.expected_data = self.expected_data
        self.vehicle_controller.tcp_client.enable_send = True
        self.expected_data = []

    def update_values(self):
        for key, value in self.vehicle_controller.tcp_client.encoded_message:
            if key in self.current_values.keys():
                self.current_values[key] = value
                self.update_dict(key, value)
                print(f"NEW: {key} : {value}")

                continue

            if key == "photo":
                image = Image.open(io.BytesIO(value))
                image = image.rotate(180)

                img_byte_arr = io.BytesIO()
                image.save(img_byte_arr, format='JPEG')
                img_byte_arr = img_byte_arr.getvalue()                

                qImg = QtGui.QImage.fromData(img_byte_arr)
                pixmap = QtGui.QPixmap.fromImage(qImg)
                self.camera_image_label.setPixmap(pixmap)
        # print(self.vehicle_controller.tcp_client.received_byte_message)    

    def update_dict(self, key, value):
        if key == "temperature":
            self.temperature_label.setText(f"Temperature: {value}°C")
        if key == "humidity":
            self.humidity_label.setText(f"Humidity: {value}%")        
        if key == "light intensity":
            self.light_intensity_label.setText(f"Light intensity: {value}lux")                  
        if key == "atmospheric pressure":
            self.air_pressure_label.setText(f"Air pressure: {value}hPa")        
        if key == "distance front":
            if value > 1000.0:
                self.front_label.setText(f"Front: too far")
                return
            self.front_label.setText(f"Front: {value}cm") 
        if key == "distance right":
            if value > 1000.0:
                self.right_label.setText(f"Right: too far")
                return            
            self.right_label.setText(f"Right: {value}cm") 
        if key == "distance left":
            if value > 1000.0:
                self.left_label.setText(f"Left: too far")
                return               
            self.left_label.setText(f"Left: {value}cm") 
        if key == "distance back":
            if value > 1000.0:
                self.back_label.setText(f"Back: too far")
                return             
            self.back_label.setText(f"Back: {value}cm")                                                     

    def tcp_connect(self):
        ip = self.ip_text_box.text()
        port = self.port_text_box.text()
        self.vehicle_controller.tcp_client.connect(ip=ip, port=int(port))

        self.vehicle_controller.tcp_client.client_socket.sendall(int.to_bytes(VehicleController.ADDRESSES["node_addr"], 1, byteorder='big'))

        self.send_button.setEnabled(True)

        self.get_photo()

        QtWidgets.QMessageBox.about(MainWindow, "Success", "Connected!")

    def add_command(self):
        command_name = self.commands_combo_box.currentText()
        new_item = QtWidgets.QListWidgetItem()
        
        new_item.setStatusTip(command_name)

        if(command_name in VehicleController.MOVE_COMMANDS.keys()):
            if not self.move_value_text_box.text().isnumeric():
                QtWidgets.QMessageBox.about(MainWindow, "Error", "Only numbers are accepted!")
                return

            new_item.setText(command_name + ", value: " + self.move_value_text_box.text())
            new_item.setWhatsThis(self.move_value_text_box.text())

        else:
            new_item.setText(command_name)

        self.actions_list.addItem(new_item)

    def delete_command(self):
        listItems = self.actions_list.selectedItems()
        # print(listItems[0].text())
        # print(listItems[0].whatsThis())
        # print(listItems[0].statusTip())
        if not listItems:
            return
        
        for item in listItems:
            self.actions_list.takeItem(self.actions_list.row(item))

    def send_actions(self):

        send_byte = b''
        if(self.actions_list.count() <= 0):
            return

        for i in range(self.actions_list.count()):
            command = self.actions_list.item(i).statusTip()

            if(command in VehicleController.MOVE_COMMANDS.keys()):
                send_byte += self.toByte(VehicleController.MOVE_COMMANDS[command]) + self.toByte(int(self.actions_list.item(i).whatsThis()))
                continue

            send_byte += self.toByte(VehicleController.DATA_COMMANDS[command])

            try:
                value = command.split("get ")
                self.expected_data.append(value[1])
            except:
                pass     

            if command.split(" ")[-1] == "photo":
                self.expected_data.append("photo")     

            if command.split(" ")[-1] == "data":
                for key in self.current_values.keys():
                    self.expected_data.append(key)

        self.vehicle_controller.tcp_client.encoded_message = []
        self.vehicle_controller.tcp_client.send_byte_message = send_byte
        self.vehicle_controller.tcp_client.expected_data = self.expected_data
        self.vehicle_controller.tcp_client.enable_send = True
        self.expected_data = []


    def toByte(self, data):
        return int.to_bytes(data, 1, byteorder='big')
        


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui_vehicle_controller = Ui_VehicleController(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())