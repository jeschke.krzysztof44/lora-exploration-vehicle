/*
    NetworkServer.h
    
    Author:     Krzysztof Jeschke
    Created:    25.04.2022
*/

#pragma once

#ifndef __NetworkServer_H__
#define __NetworkServer_H__

#include <string.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include "lwip/sockets.h"
#include <lwip/netdb.h>


#include "../../include/config.h"
#include "../../include/ttn_private.h"

// AP
#define EXAMPLE_ESP_WIFI_SSID      "wifi_server"
#define EXAMPLE_ESP_WIFI_PASS      "12345678"
#define EXAMPLE_ESP_WIFI_CHANNEL   1
#define EXAMPLE_MAX_STA_CONN       4

// TCP Server
#define IP_ADDRESS                  "192.168.4.1"
#define PORT                        10001
#define KEEPALIVE_IDLE              5
#define KEEPALIVE_INTERVAL          5
#define KEEPALIVE_COUNT             3

class NetworkServer
{

// variables
public:

    //char receiveBuffer[TCP_BUFFER_SIZE];
    uint8_t receivedFrameSize = 0;

private:
    static constexpr const char *TAG = "NetworkServer";

    int currentSocket;

    // char send_buffer[TCP_BUFFER_SIZE];
    // uint8_t sent_frame_size = 0;

    bool enableSending = false;
    bool enableReceiving = false;

    // TcpData *tcpData;

// methods
public:
    NetworkServer();
    ~NetworkServer();

    esp_err_t init();
    void wifi_init_softap();
    void tcp_server_task();

private:
    static void wifi_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data);
    void do_retransmit(const int sock);
};

#endif