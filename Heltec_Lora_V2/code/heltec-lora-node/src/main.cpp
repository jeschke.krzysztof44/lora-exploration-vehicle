
#include "config.h"
#include "NetworkServer.h"

NetworkServer networkServer;

extern "C" void app_main(void)
{
    networkServer.init();
    networkServer.wifi_init_softap();
    networkServer.tcp_server_task();

}
